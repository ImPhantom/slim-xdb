﻿using System;
using System.Text;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using Discord.Commands;
using Humanizer;
using sXDB.Core;
using sXDB.Services;

namespace sXDB.Modules
{
    [Name("Bot")]
    [Group("bot")]
    [RequireOwner]
    public class Bot : XenoModuleBase
    {
        public LoggingService _logger { get; set; }

        [Command("logs")]
        [Name("bot logs <opt_line_count>"), Summary("Tails the lastest log file.")]
        public async Task GetBotLogs(int lines = 10)
        {
            var logs = await _logger.TailLogsAsync(lines);
            await ReplyAsync($":small_blue_diamond: **Last {logs.Count()} lines of** `latest.log`\r\n```prolog\r\n{string.Join("\r\n", logs)}\r\n```");
        }

        [Command("proc")]
        [Name("bot proc"), Summary("Returns information about the bot process.")]
        public async Task GetProcessInfo()
        {
            var _proc = Process.GetCurrentProcess();
            var heapSize = Math.Round(GC.GetTotalMemory(true) / (1024.0 * 1024.0), 2);

            var entryCommand = $"{_proc.ProcessName} {Environment.CommandLine.Replace(AppContext.BaseDirectory, "")}";

            var processInfo = new StringBuilder();
            processInfo.AppendLine("Active Process:");
            processInfo.AppendLine($" {_proc.ProcessName}");
            processInfo.AppendLine($"  Threads: {_proc.Threads.Count}");
            processInfo.AppendLine($"  Heap Size: {heapSize}MB");
            processInfo.AppendLine($"  Entry: \"{entryCommand}\"");
            processInfo.AppendLine($"  Uptime: {(DateTime.Now - _proc.StartTime).Humanize(3)}");


            await ReplyAsync($":small_blue_diamond: **Current Process Information**```ml\r\n{processInfo.ToString()}\r\n```");
        }
    }
}
