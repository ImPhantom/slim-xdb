﻿using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using sXDB.Core;
using sXDB.Services;
using sXDB.Core.Extensions;

namespace sXDB.Modules
{
    [Name("General")]
    public class Chat : XenoModuleBase
    {
        public ChatService _chatService { get; set; }

        [Name("Quick Replies")]
        [Group("quickr"), Alias("qr")]
        [RequireContext(ContextType.Guild)]
        public class QuickReplies : XenoModuleBase
        {
            public ChatService _chatService { get; set; }

            [Command("list")]
            [Name("quickr list"), Summary("Lists all available quick replies for the guild.")]
            [RequireUserPermission(GuildPermission.Administrator)]
            public async Task ListQuickReplies()
            {
                var quickReplies = await _chatService.GetQuickReplies(Context.Guild.Id);

                if(quickReplies == null || !quickReplies.Replies.Any())
                {
                    await ReplyErrorAsync($"You can use the command:\n `{Context.Guild.GetPrefix()}quickr + <reply-name> <'intial trigger'> <response>`\nto create a new quick reply.", "No quick replies for this guild!");
                    return;
                }

                var shortList = new StringBuilder();

                var nameMax = (quickReplies.Replies.Select(_reply => _reply.ReplyName).ToArray()).Max(_name => _name.Length) + 4;
                foreach (var reply in quickReplies.Replies)
                    shortList.AppendLine($"'{($"{reply.ReplyName}'").PadRight(nameMax)}  =>  \"{reply.Triggers.Count()} triggers\"");

                await ReplyAsync($":speech_balloon: **All quick replies for this guild:**\n```ml\n{shortList.ToString()}\n```\nUse `{Context.Guild.GetPrefix()}quickr list <reply_name>` to see more info about a quick reply.");
            }

            [Command("list")]
            [Name("quickr list <reply_name>"), Summary("Lists information for a specific quick reply.")]
            [RequireUserPermission(GuildPermission.Administrator)]
            public async Task ListQuickReplyDetails(string replyName)
            {
                var quickReplies = await _chatService.GetQuickReplies(Context.Guild.Id);

                if (quickReplies == null || !quickReplies.Replies.Any(_reply => _reply.ReplyName == replyName))
                {
                    await ReplyErrorAsync("That quick reply does not exist in this guild.");
                    return;
                }

                var reply = quickReplies.Replies.First(_reply => _reply.ReplyName == replyName);

                var channels = new List<string>();
                foreach (var channel in reply.AllowedChannels)
                    channels.Add($"<#{channel}>");
                var channelString = (reply.AllowedChannels.Any()) ? $"**Channels:**\n{string.Join(", ", channels)}" : "";


                var responseMessage = $"**Response:**\n{reply.Response}\n\n**Triggers:**\n```\n'{string.Join("'\n'", reply.Triggers)}'\n```\n{channelString}";

                await ReplyPrettyEmbedAsync(responseMessage, $"Quick Reply Details: '{replyName}'");
            }

            [Command("add"), Alias("+")]
            [Name("quickr add <reply_name> <inital_trigger> <response>"), Summary("Adds a new quick reply.")]
            [RequireUserPermission(GuildPermission.Administrator)]
            public async Task AddQuickReply(string replyName, string replyTrigger, [Remainder] string response)
            {
                var result = await _chatService.AddQuickReply(Context.Guild.Id, replyName, replyTrigger, response);

                if (result)
                {
                    await Context.Message.DeleteAsync();
                    await Context.Channel.SendEmbedAsync($":ok_hand: Successfully added quick reply `{replyName}` with 1 trigger.");
                }
            }

            [Command("edit"), Alias("update")]
            [Name("quickr edit <#channel> <#channel> <more_channels>"), Summary("Restrict quick reply to certain channels.")]
            [RequireUserPermission(GuildPermission.Administrator)]
            public async Task EditQuickReply(string replyName, params SocketGuildChannel[] channels)
            {
                var channelIds = new List<ulong>();
                foreach (var channel in channels)
                    channelIds.Add(channel.Id);

                var result = await _chatService.EditReplyChannels(Context.Guild.Id, replyName, channelIds);
                if (result != null)
                {
                    await Context.Message.DeleteAsync();
                    var response = new StringBuilder($":ok_hand: **Updated channel whitelist for all `{replyName}` triggers:**\n\n");
                    foreach (var modifiedChannel in result.ModifiedChannels)
                        response.Append($"{((modifiedChannel.Value) ? ":arrow_right:" : ":regional_indicator_x:")}  <#{modifiedChannel.Key}>\n");

                    await Context.Channel.SendEmbedAsync(response.ToString());
                }
                else
                    await ReplyErrorAsync($"Unable to edit quick reply: {replyName}");
            }

            [Command("edit"), Alias("update")]
            [Name("quickr edit <'trigger one'> <'trigger two'>"), Summary("Add new triggers to a certain quick reply.")]
            [RequireUserPermission(GuildPermission.Administrator)]
            public async Task EditQuickReply(string replyName, params string[] triggers)
            {
                var result = await _chatService.EditReplyTriggers(Context.Guild.Id, replyName, triggers.ToList());

                if (triggers.Count() > 15)
                {
                    await ReplyErrorAsync("You can only add 15 triggers at a time.");
                    return;
                }

                if (result != null)
                {
                    var added = result.ModifiedTriggers.Where(_trigger => _trigger.Value).Count();
                    var removed = result.ModifiedTriggers.Where(_trigger => !_trigger.Value).Count();

                    await Context.Message.DeleteAsync();
                    var response = new StringBuilder($":ok_hand: **Updated triggers for** `{replyName}`\n\n");
                    response.AppendLine($":white_check_mark: {added} triggers added.");
                    response.AppendLine($":white_check_mark: {removed} triggers removed.");
                    await Context.Channel.SendEmbedAsync(response.ToString());
                }
                else
                    await ReplyErrorAsync($"Unable to edit quick reply triggers for: '{replyName}'");
            }

            [Command("edit-response"), Alias("update-response")]
            [Name("quickr edit-response <reply_name> <response>"), Summary("Change the response for a quick reply.")]
            [RequireUserPermission(GuildPermission.Administrator)]
            public async Task EditQuickReply(string replyName, [Remainder] string response)
            {
                var result = await _chatService.EditReplyResponse(Context.Guild.Id, replyName, response);

                if (result)
                {
                    await Context.Message.DeleteAsync();
                    await Context.Channel.SendEmbedAsync($":ok_hand: Now responding to `{replyName}` triggers with:\n\n'{response}'");
                }
                else
                    await ReplyErrorAsync($"Unable to edit quick reply response for: '{replyName}'");
            }

            [Command("remove"), Alias("-")]
            [Name("quickr remove <reply_name>"), Summary("Removes a certain quick reply.")]
            [RequireUserPermission(GuildPermission.Administrator)]
            public async Task RemoveQuickReply(string replyName)
            {
                var result = await _chatService.RemoveQuickReply(Context.Guild.Id, replyName);

                if (result)
                {
                    await Context.Message.DeleteAsync();
                    await Context.Channel.SendEmbedAsync($":ok_hand: Removed quick reply `{replyName}` and all of its triggers");
                }
                else
                    await ReplyErrorAsync($"Unable to remove quick reply: '{replyName}'");
            }
        }
    }
}
