﻿using System.Net;
using System.Net.Http;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Generic;
using Discord;
using Discord.Commands;
using Newtonsoft.Json;
using sXDB.Core;
using sXDB.Core.Models;
using sXDB.Core.Preconditions;
using sXDB.Services;
using System;

namespace sXDB.Modules
{
    [Group("xmp")]
    [RequireContext(ContextType.Guild)]
    public class Xeno : XenoModuleBase
    {
        public XMPQueueService _queueService { get; set; }

        [Command("queue")]
        [Name("xmp queue"), Summary("Displays the last tick information for the XMP Queue Service.")]
        [RequireRole("Owners")]
        public async Task Test()
        {
            var lastRequest = _queueService.GetLastRequest();

            if (lastRequest != null)
            {
                var tasks = new List<string>();
                foreach (var result in lastRequest.Results)
                    tasks.Add($"Task #{result.TaskId}    ->    Result::{Enum.GetName(typeof(TaskResult), result.ResultValue)}");

                var response = $"**Response:**\n- Status: **{lastRequest.StatusCode}** ({lastRequest.Response.State.ToLower()})\n- Response: `{lastRequest.Response.Response}`\n\n**Handled Tasks:**\n```\n{string.Join("\n", tasks)}\n```";

                await ReplyPrettyEmbedAsync(response, lastRequest.Timestamp, "Last meaningful 'Queue::Core' tick:");
            }
            else
                await ReplyErrorAsync("There have been no meaningful Queue::Core ticks. :frowning2:");
        }

        [Command("stats")]
        [Name("xmp stats"), Summary("Displays your staffing statistics.")]
        [RequireRole("Owners", "Admin", "Senior Moderator", "Moderator", "Trial-Mod")]
        public async Task FetchStaffingStatistics()
        {
            var coreConfig = await (new Configuration()).Load();
            var stopwatch = Stopwatch.StartNew();

            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("token", coreConfig.XenoApiToken),
                new KeyValuePair<string, string>("discord-user-id", Context.User.Id.ToString())
            });

            using (var client = new HttpClient())
            {
                var responseStream = await client.PostAsync("https://dev.xeno.gg/@/statistics/", content);
                if (responseStream.StatusCode == HttpStatusCode.OK)
                {
                    var responseString = await responseStream.Content.ReadAsStringAsync();
                    var response = JsonConvert.DeserializeObject<StatisticsResponse>(responseString);

                    if (string.IsNullOrEmpty(response.State))
                    {
                        var embed = new EmbedBuilder()
                            .WithAuthor($"{response.DisplayName}'s Statistics", response.AvatarUrl)
                            .WithFooter($"Fetch took: {stopwatch.ElapsedMilliseconds}ms")
                            .WithColor(Global.RandomColorFromPalette());

                        embed.AddField("Current Week", $"{response.Tickets.CurrentWeek} tickets ({response.Sessions.CurrentWeek} hours)", true);
                        embed.AddField("Last Week", $"{response.Tickets.LastWeek} tickets ({response.Sessions.LastWeek} hours)", true);
                        embed.AddField("All Time", $"{response.Tickets.AllTime} tickets ({response.Sessions.AllTime} hours)", true);

                        stopwatch.Stop();
                        await ReplyAsync(embed: embed.Build());
                    }
                    else
                        await ReplyNoticeAsync("Make sure that you've linked your discord account on the management portal.", "No associated management portal account found!");
                }
            }      
        }
    }
}
