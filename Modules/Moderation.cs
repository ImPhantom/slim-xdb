﻿using System;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Humanizer;
using sXDB.Core;
using sXDB.Services;
using sXDB.Core.Extensions;

namespace sXDB.Modules
{
    [Name("Moderation")]
    public class Moderation : XenoModuleBase
    {
        public ChatService _chatService { get; set; }

        [Command("clean")]
        [Name("clean <opt_amount>"), Summary("Cleans messages from the channel")]
        [RequireBotPermission(ChannelPermission.ManageMessages)]
        [RequireUserPermission(ChannelPermission.ManageMessages)]
        public async Task CleanMessages(int amountToClean = 10)
        {
            if (amountToClean <= 0)
                return;

            var messages = await Context.Channel.GetMessagesAsync(Context.Message, Direction.Before, amountToClean).FlattenAsync();
            var filteredMessages = messages.Where(_message => (DateTimeOffset.UtcNow - _message.Timestamp).TotalDays <= 14);

            var messageCount = filteredMessages.Count();
            if (messageCount > 0)
            {
                await (Context.Channel as ITextChannel).DeleteMessagesAsync(filteredMessages);
                await ReplyThenRemoveAsync($":ok_hand: **Removed {messageCount} {(messageCount > 1 ? "messages" : "message")}.** (may take some time)");
            }
            else
                await ReplyThenRemoveAsync($":anger: **No messages to delete.**");

            await Context.Message.DeleteAsync();
        }

        [Command("blacklist")]
        [Name("blacklist <@user>"), Summary("Adds or removes a user from the bot blacklist for this guild.")]
        [RequireUserPermission(GuildPermission.KickMembers)]
        [RequireContext(ContextType.Guild)]
        public async Task BlacklistUser(SocketGuildUser user)
        {
            var result = await _chatService.BlacklistUser(Context.Guild.Id, user.Id);

            if (result)
                await ReplyOkAsync();
            else
                await ReplyErrorAsync($"Failed to blacklist {user}");
        }

        [Name("Temporary Bans")]
        [Group("tempban"), Alias("tban")]
        [RequireContext(ContextType.Guild)]
        public class TemporaryBans : XenoModuleBase
        {
            public ModerationService _modService { get; set; }
            public GuildService _guildService { get; set; }

            [Command]
            [Name("tempban <@user> <1<h/d/w>> <reason>"), Summary("Temporarily bans a user from the guild.")]
            [RequireBotPermission(GuildPermission.BanMembers)]
            [RequireUserPermission(GuildPermission.BanMembers)]
            public async Task TemporarilyBanUser(SocketGuildUser guildUser, TimeSpan length, [Remainder] string reason = "")
            {
                var _reason = (string.IsNullOrEmpty(reason)) ? "no reason specified" : reason;
                if (!guildUser.IsBot)
                    await (await guildUser.GetOrCreateDMChannelAsync()).SendMessageAsync($":hammer: You have been banned from __{Context.Guild.Name}__ for **{length.Humanize()}**\r\n**Reason:** {_reason}");

                await _modService.TemporarilyBan(Context.Guild.Id, guildUser.Id, Context.User.Id, (DateTime.UtcNow + length), reason);

                var logChannel = await Context.Guild.GetLoggingChannel();
                if (logChannel != null)
                    await logChannel.SendMessageAsync($":hammer: `{Context.User.Username}#{Context.User.Discriminator}` has banned {guildUser.Mention} for **{length.Humanize()}**\r\n**Reason:** {_reason}");

                await ReplyOkAsync();
            }

            [Command("list"), Alias("active")]
            [Name("tempban list"), Summary("Lists all active temporary bans for the guild.")]
            public async Task DisplayTemporaryBans()
            {
                var tempBans = _modService.GetActiveTemporaryBans(Context.Guild.Id);

                if (tempBans != null)
                {
                    var formattedBanList = new StringBuilder();
                    var tagMaxWidth = (tempBans.Select(_ban => _ban.BannedUserTag).ToArray()).Max(_tag => _tag.Length) + 5;
                    var idMaxWidth = (tempBans.Select(_ban => _ban.BanId).ToArray()).Max(_id => _id.ToString().Length) + 3;

                    formattedBanList.AppendLine($"{("Id").PadRight(idMaxWidth - 1)}| {("Banned User").PadRight(tagMaxWidth - 2)}| Time Left");
                    foreach (var ban in tempBans)
                        formattedBanList.AppendLine($"#{ban.BanId.ToString().PadRight(idMaxWidth)}{ban.BannedUserTag.PadRight(tagMaxWidth)}{(ban.DateUnbanned - DateTime.UtcNow).Humanize()}");

                    var prefix = await _guildService.GetPrefix(Context.Guild.Id);
                    var embed = new EmbedBuilder()
                        .WithTitle($":small_blue_diamond: Active temporary bans in {Context.Guild.Name}")
                        .WithDescription($"```cs\r\n{ formattedBanList.ToString()}\r\n```")
                        .WithColor(Global.RandomColorFromPalette())
                        .WithFooter($"Use \"{prefix}tempban lookup <ban_id>\" to see full ban information.");

                    await ReplyAsync(embed: embed.Build());
                } else
                    await ReplyNoticeAsync("There are no temporary bans for this guild!");
            }

            [Command("lookup")]
            [Name("tempban lookup <ban_id>"), Summary("Displays information about a specific ban.")]
            public async Task LookupActiveTemporaryBan(int banId)
            {
                var ban = _modService.GetTemporaryBan(banId);
                if (ban != null)
                {
                    var embed = new EmbedBuilder()
                    .WithTitle($":no_entry_sign: {ban.BannedUserTag}")
                    .WithDescription(ban.Reason)
                    .WithColor(Global.RandomColorFromPalette())
                    .WithTimestamp(ban.DateBanned);

                    var admin = Context.Client.GetUser(ban.BanningUserId);
                    embed.AddField("Banned By:", $"`{admin}`");
                    embed.AddField("Ban Length:", (ban.DateUnbanned - ban.DateBanned).Humanize(), true);
                    embed.AddField("Time Remaining:", (ban.DateUnbanned - DateTime.UtcNow).Humanize(3, minUnit: Humanizer.Localisation.TimeUnit.Second), true);

                    await ReplyAsync(embed: embed.Build());
                } else
                    await ReplyErrorAsync("No temporary ban exists for that ID.");
            }
        }

        [Name("Muting")]
        [RequireContext(ContextType.Guild)]
        public class Muting : XenoModuleBase
        {
            public ModerationService _modService { get; set; }
            public GuildService _guildService { get; set; }

            [Command("mutes")]
            [Name("mutes"), Summary("Lists all active mutes within the guild.")]
            [RequireUserPermission(GuildPermission.Administrator)]
            public async Task ListMutes()
            {
                if(_modService.HasActiveMutes(Context.Guild.Id))
                {
                    var mutes = await _modService.GetActiveMutes(Context.Guild.Id);
                    var formattedList = new StringBuilder();
                    foreach (var mute in mutes)
                    {
                        var length = "Infinite";
                        if (mute.DateUnmuted.HasValue)
                            length = (mute.DateUnmuted.Value - DateTime.UtcNow).Humanize();

                        formattedList.AppendLine($"**User:** <@{mute.MutedUserId}>    ===>   **Time Left:** `{length}`");
                    }

                    var embed = new EmbedBuilder()
                        .WithTitle($"Active mutes in {Context.Guild.Name}")
                        .WithDescription(formattedList.ToString())
                        .WithColor(Global.RandomColorFromPalette());

                    await ReplyAsync(embed: embed.Build());
                }
                else
                    await ReplyNoticeAsync("There are no active mutes in this guild!");
            }

            [Command("mute")]
            [Name("mute <@user> <reason>"), Summary("Mutes a user.")]
            [RequireUserPermission(GuildPermission.KickMembers)]
            public async Task MuteUser(SocketGuildUser user, [Remainder] string reason)
            {
                var result = await _modService.MuteUserAsync(Context.Guild.Id, user.Id, Context.User.Id, reason);
                if(result)
                {
                    var logChannel = await Context.Guild.GetLoggingChannel();
                    if(logChannel != null)
                        await logChannel.SendMessageAsync($":mute: **{user.Username}#{user.Discriminator}** has been muted by {Context.User.Mention}:\nReason: `{reason}`");
                    await (await user.GetOrCreateDMChannelAsync()).SendMessageAsync($":mute: You have been muted in {Context.Guild.Name} by **{Context.User}**\n**Reason:** `{reason}`");
                    await ReplyOkAsync();
                } else
                    await ReplyErrorAsync("Either the user is already muted, or the guilds 'Muted' role is not set.", "Unable to mute user!");
            }

            [Command("tempmute")]
            [Name("tempmute <@user> <1<h/d/w>> <reason>"), Summary("Mutes a user temporarily.")]
            [RequireUserPermission(GuildPermission.KickMembers)]
            public async Task TempMuteUser(SocketGuildUser user, TimeSpan length, [Remainder] string reason)
            {
                var result = await _modService.MuteUserAsync(Context.Guild.Id, user.Id, Context.User.Id, reason, length);
                if (result)
                {
                    var logChannel = await Context.Guild.GetLoggingChannel();
                    if (logChannel != null)
                        await logChannel.SendMessageAsync($":mute: **{user.Username}#{user.Discriminator}** has been temporarily muted by {Context.User.Mention} for {length.Humanize()}:\nReason: `{reason}`");
                    await (await user.GetOrCreateDMChannelAsync()).SendMessageAsync($":mute: You have been tempoarily muted in {Context.Guild.Name} by **{Context.User}**\n**Reason:** `{reason}`\n**Length:** {length.Humanize()}");
                    await ReplyOkAsync();
                } else
                    await ReplyErrorAsync("Either the user is already muted, or the guilds 'Muted' role is not set.", "Unable to mute user!");
            }

            [Command("unmute")]
            [Name("unmute <@user>"), Summary("Unmutes a user if muted.")]
            [RequireUserPermission(GuildPermission.KickMembers)]
            public async Task UnmuteUser(SocketGuildUser user)
            {
                var result = await _modService.UnmuteUserAsync(Context.Guild.Id, user.Id);
                if (result)
                {
                    var logChannel = await Context.Guild.GetLoggingChannel();
                    if (logChannel != null)
                        await logChannel.SendMessageAsync($":loud_sound: **{user.Username}#{user.Discriminator}** has been unmuted.");
                    await (await user.GetOrCreateDMChannelAsync()).SendMessageAsync($":loud_sound: You have been unmuted in {Context.Guild.Name}");
                    await ReplyOkAsync();
                } else
                    await ReplyErrorAsync("No mutes stored in this guild for that user.", "Unable to unmute user!");
            }
        }
    }
}
