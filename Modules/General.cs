﻿using System;
using System.Text;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Humanizer;
using sXDB.Core;
using sXDB.Services;
using System.Collections.Generic;

namespace sXDB.Modules
{
    [Name("General")]
    public class General : XenoModuleBase
    {
        public DiscordSocketClient _client { get; set; }
        public CommandService _commandService { get; set; }
        public GuildService _guildService { get; set; }

        [Command("8ball")]
        [Name("8ball <question>"), Summary("Ask the magic eight ball a question.")]
        public async Task AskMagicEightBall([Remainder] string question)
        {
            string[] responses = { "It is certain", "It is decidedly so", "Without a doubt", "Yes, definitely", "You may rely on it", "As I see it, yes", "Most likely", "Outlook good", "Yes", "Signs point to yes", "Reply hazy try again", "Ask again later", "Better not tell you now", "Cannot predict now", "Concentrate and ask again", "Don't count on it", "My reply is no", "My sources say no", "Outlook not so good", "Very doubtful" };
            var response = responses[(new Random()).Next(responses.Length)];
            await ReplyAsync($":8ball: {response}");
        }

        [Command("user"), Alias("userinfo")]
        [Name("user <optional_user>"), Summary("Displays information about the user.")]
        [RequireContext(ContextType.Guild)]
        public async Task DisplayUserInformation(SocketGuildUser guildUser = null)
        {
            if (guildUser == null)
                guildUser = Context.User as SocketGuildUser;

            var embed = new EmbedBuilder()
                .WithAuthor($"{guildUser.Username} ({guildUser.Status.ToString()})", guildUser.GetAvatarUrl())
                .WithColor(Global.RandomColorFromPalette())
                .WithCurrentTimestamp();

            var creationDaysAgo = (DateTime.Now - guildUser.CreatedAt).TotalDays;
            var joinedDaysAgo = (DateTime.Now - guildUser.JoinedAt.Value).TotalDays;

            embed.AddField("Username:", $"`{guildUser.Username}#{guildUser.Discriminator}`", true);
            embed.AddField("User ID:", $"`{guildUser.Id}`", true);
            embed.AddField("Account Creation:", $"{guildUser.CreatedAt.ToString("M/d/yyyy")} (**{Math.Round(creationDaysAgo)}** days ago)", false);
            embed.AddField("Joined Guild:", $"{guildUser.JoinedAt.Value.ToString("M/d/yyyy")} (**{Math.Round(joinedDaysAgo)}** days ago)", false);

            await ReplyAsync(embed: embed.Build());
        }

        [Command("guild"), Alias("guildinfo")]
        [Name("guild"), Summary("Displays guild information.")]
        [RequireContext(ContextType.Guild)]
        public async Task DisplayGuildInformation()
        {
            var embed = new EmbedBuilder()
                .WithAuthor($"{Context.Guild.Name} ({Context.Guild.VoiceRegionId})", Context.Guild.IconUrl)
                .WithColor(Global.RandomColorFromPalette())
                .WithCurrentTimestamp();

            embed.AddField("Owner:", Context.Guild.Owner.Mention, true);
            embed.AddField("Guild ID:", $"`{Context.Guild.Id}`", true);
            embed.AddField("Created:", $"{Context.Guild.CreatedAt.ToString("M/dd/yyyy")} (**{Math.Round((DateTime.Now - Context.Guild.CreatedAt).TotalDays)}** days ago)", true);
            embed.AddField("AFK Timeout:", $"{TimeSpan.FromSeconds(Context.Guild.AFKTimeout).Minutes} minutes", true);
            embed.AddField("Roles:", Context.Guild.Roles.Count.ToString(), true);
            embed.AddField("Users:", Context.Guild.MemberCount, true);

            await ReplyAsync(embed: embed.Build());
        }

        [Command("info"), Alias("bot")]
        [Name("info"), Summary("Displays information about the sXDB instance.")]
        public async Task DisplayBotInformation()
        {
            var application = await Context.Client.GetApplicationInfoAsync();

            var embed = new EmbedBuilder()
                .WithColor(Global.RandomColorFromPalette())
                .WithAuthor(Context.Client.CurrentUser.Username, Context.Client.CurrentUser.GetAvatarUrl())
                .WithFooter($"{RuntimeInformation.OSDescription} ({RuntimeInformation.FrameworkDescription})");

            embed.AddField("Author:", $"{application.Owner.Mention} (`{application.Owner}`)", false);
            embed.AddField("App Version:", $"sXDB (`v{Global.Version}`)", true);
            embed.AddField("Library Version:", $"Discord.Net (`{DiscordConfig.Version}`)", true);
            embed.AddField("Uptime:", $"`{(DateTime.Now - Process.GetCurrentProcess().StartTime).Humanize(2)}`", false);

            await ReplyAsync(embed: embed.Build());
        }

        [Command("ping"), Alias("rtt")]
        [Name("ping"), Summary("Evaluates message round trip time, and API Latency.")]
        public async Task Ping()
        {
            var testMessage = await ReplyAsync("Evaluating latency...");

            var rtt = (testMessage.Timestamp - Context.Message.Timestamp).TotalMilliseconds;
            await testMessage.ModifyAsync(x => x.Content = $"**Pong!**\r\nRound Trip: `{rtt}ms`\r\nAPI Latency: `{_client.Latency}ms`");
        }

        [Command("help")]
        [Name("help"), Summary("Replies with all available commands.")]
        public async Task DisplayHelp()
        {
            string prefix = (new Configuration()).DefaultPrefix;
            if(Context.Guild != null)
                prefix = await _guildService.GetPrefix(Context.Guild.Id);
            var helpString = new StringBuilder();
            var overflowString = new StringBuilder();
            foreach (var module in _commandService.Modules)
            {
                var moduleCommands = new Dictionary<string, string>();
                foreach (var command in module.Commands)
                {
                    var result = await command.CheckPreconditionsAsync(Context);
                    if (result.IsSuccess)
                        moduleCommands.TryAdd($"{prefix}{command.Name}", command.Summary);
                }

                var moduleName = module.Name;
                if (module.IsSubmodule)
                    moduleName = $"{module.Name} ({module.Parent.Name})";

                var moduleHelp = new StringBuilder().AppendLine($"> {moduleName}");

                if (moduleCommands.Any())
                {
                    var commandMax = (moduleCommands.Select(_command => _command.Key).ToArray()).Max(_key => _key.Length) + 2;
                    foreach (var command in moduleCommands)
                        moduleHelp.AppendLine($"{command.Key.PadRight(commandMax)} -> {command.Value}");

                    moduleHelp.AppendLine();

                    if (helpString.Length + moduleHelp.Length > 1900)
                        overflowString.Append(moduleHelp.ToString());
                    else
                        helpString.Append(moduleHelp.ToString());
                }
            }

            await ReplyAsync($":small_blue_diamond: Available commands:\r\n```md\r\n{helpString.ToString()}\r\n```");
            if (overflowString.ToString() != string.Empty)
                await ReplyAsync($"```md\r\n{overflowString.ToString()}\r\n```");
        }
    }
}
