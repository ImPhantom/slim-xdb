﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Discord;
using Discord.Commands;
using sXDB.Core;
using sXDB.Services;

namespace sXDB.Modules
{
    [Name("Tags")]
    [RequireContext(ContextType.Guild)]
    public class Tags : XenoModuleBase
    {
        public TagService tagService { get; set; }

        [Command("tag"), Alias("t")]
        [Name("tag <tag_name>"), Summary("Displays tag information if it exists.")]
        public async Task DisplayTag(string tagName)
        {
            var tag = await tagService.GetTag(Context.Guild.Id, tagName);
            if(tag != null)
            {
                var embed = new EmbedBuilder()
                    .WithTitle($"**{tagName}** (tag #{tag.TagId})")
                    .WithDescription(tag.Content)
                    .WithColor(Global.RandomColorFromPalette())
                    .WithTimestamp(tag.CreationDate);

                await ReplyAsync(embed: embed.Build());
            } else
                await ReplyErrorAsync($"No tag exists under the alias `{tagName}`.");
        }

        [Command("tags")]
        [Name("tags"), Summary("Lists all available tags in the guild.")]
        public async Task ListTags()
        {
            var guildTags = await tagService.GetTags(Context.Guild.Id);
            
            List<string> tagNames = new List<string>();
            guildTags.ForEach(tag => tagNames.Add(tag.Name));

            var embed = new EmbedBuilder()
                .WithTitle($"Displaying available tags in {Context.Guild.Name}:")
                .WithDescription(string.Join(", ", tagNames))
                .WithColor(Global.RandomColorFromPalette());

            await ReplyAsync(embed: embed.Build());
        }

        [Command("createtag")]
        [Name("createtag <tag_name> <tag_content>"), Summary("Creates a new tag in the guild.")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task CreateNewTag(string tagName, [Remainder] string tagContent)
        {
            var tag = new Database.Models.Tag()
            {
                GuildId = Context.Guild.Id,
                CreatorId = Context.User.Id,
                CreationDate = DateTime.Now,
                Name = tagName,
                Content = tagContent
            };

            await tagService.CreateTag(tag);
            await ReplyAsync($":heavy_check_mark: Tag `{tagName}` created!");
        }

        [Command("removetag")]
        [Name("removetag <tag_name>"), Summary("Removes a specified tag from the guild.")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task RemoveTag(string tagName)
        {
            var result = await tagService.RemoveTag(Context.Guild.Id, tagName);

            if (!result)
                await ReplyErrorAsync($"No tag exists under the alias `{tagName}`.");
            else
                await ReplyAsync($":heavy_minus_sign: Removed tag `{tagName}`.");
        }
    }
}
