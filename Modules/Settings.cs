﻿using System;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using sXDB.Services;
using sXDB.Core.Preconditions;
using sXDB.Core;
using System.Linq;
using System.Text;

namespace sXDB.Modules
{
    [Name("Settings")]
    [RequireContext(ContextType.Guild)]
    public class Settings : XenoModuleBase
    {
        /* TODO: Command to disable logging
         *  Would have to make sure every case where a message is trying to be sent
         *  to a logging channel, that the logging channel itself is nullable
         *  (GetLoggingChannel())?.SendMessageAsync(....);
         *                       ^
         */

        [Group("set")]
        public class Set : XenoModuleBase
        {
            public GuildService _guildService { get; set; }

            [Command("prefix")]
            [Name("set prefix <new_prefix>"), Summary("Sets the bots prefix for this guild.")]
            [RequireGuildOwner]
            public async Task SetGuildPrefix(string newPrefix)
            {
                await _guildService.SetPrefix(Context.Guild.Id, newPrefix);
                await ReplyOkAsync();
            }

            [Command("logchannel")]
            [Name("set logchannel <#channel>"), Summary("Sets the bots logging channel.")]
            [RequireGuildOwner]
            public async Task SetGuildLoggingChannel(SocketTextChannel channel)
            {
                try
                {
                    var testMessage = await channel.SendMessageAsync("*this message is testing permissions*");
                    await testMessage.DeleteAsync();
                }
                catch
                {
                    await ReplyErrorAsync($"Make sure that I have access to read/send messages in {channel.Mention}", "Unable to set logging channel!");
                    return;
                }
                await _guildService.SetLoggingChannel(Context.Guild.Id, channel.Id);
                await ReplyOkAsync();
            }

            [Command("muterole")]
            [Name("set muterole <@role/role_id>"), Summary("Sets the muted role used by [p]mute")]
            [RequireGuildOwner]
            public async Task SetMuteRole(SocketRole role)
            {
                if (Context.Guild.CurrentUser.Roles.Max(_role => _role.Position) < role.Position)
                {
                    await ReplyErrorAsync("Make sure the bots highest role is above the muted role.", "Bot can't access that role!");
                    return;
                }

                await _guildService.SetMuteRole(Context.Guild.Id, role.Id);
                await ReplyOkAsync();
            }
        }
        
        [Group("blacklist"), Alias("bl")]
        public class Blacklist : XenoModuleBase
        {
            public GuildService _guildService { get; set; }

            [Command("all"), Alias("list")]
            [Name("blacklist all"), Summary("Lists all blacklisted channels")]
            [RequireGuildOwner]
            public async Task ListBlacklistedChannels()
            {
                var blacklistedChannels = await _guildService.GetBlacklistedChannels(Context.Guild.Id);
                if (blacklistedChannels != null)
                {
                    var listString = new StringBuilder();
                    blacklistedChannels.ForEach((channelId) =>
                    {
                        var channel = Context.Guild.GetTextChannel(channelId);
                        listString.AppendLine(channel.Mention);
                    });

                    await ReplyAsync($":small_blue_diamond: **These channels are currently blacklisted:**\r\n{listString.ToString()}");
                }
                else
                    await ReplyAsync(":anger: **There are no blacklisted channels!**");
                
            }

            [Command("add"), Alias("+")]
            [Name("blacklist add <#channel>"), Summary("Adds a text channel to the blacklist")]
            [RequireGuildOwner]
            public async Task AddChannelToBlacklist(SocketTextChannel textChannel)
            {
                await _guildService.AddBlacklistedChannel(Context.Guild.Id, textChannel.Id);
                await ReplyOkAsync();
            }

            [Command("remove"), Alias("-")]
            [Name("blacklist remove <#channel>"), Summary("Removes a text channel from the blacklist")]
            [RequireGuildOwner]
            public async Task RemoveChannelFromBlacklist(SocketTextChannel textChannel)
            {
                var result = await _guildService.RemoveBlacklistedChannel(Context.Guild.Id, textChannel.Id);
                if (result)
                    await ReplyOkAsync();
                else
                    await ReplyErrorAsync("Channel was not in the blacklist!");
            }
        }
    }
}
