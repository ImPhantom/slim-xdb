﻿using System.Net;
using System.Diagnostics;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using sXDB.Core;
using sXDB.Services;
using sXDB.SourceQuery;
using sXDB.Core.Extensions;

namespace sXDB.Modules
{
    public class Steam : XenoModuleBase
    {
        public GuildService _guildService { get; set; }

        [Command("server")]
        [Name("server <ip:port>"), Summary("Querys a source server for its basic info.")]
        [RequireContext(ContextType.Guild)]
        public async Task QueryForServerDetails(string ip)
        {
            var stopwatch = Stopwatch.StartNew();

            var splitIp = ip.Split(':');
            if(splitIp.Length != 2)
            {
                await ReplyErrorAsync("Invalid IP Address/Port format. Required: `127.0.0.1:12345`");
                return;
            }

            var endpoint = new IPEndPoint(IPAddress.Parse(splitIp[0]), int.Parse(splitIp[1]));
            var serverDetails = (new Query()).QueryServer(endpoint);

            var queryTime = stopwatch.ElapsedMilliseconds;
            stopwatch.Stop();

            var embed = new EmbedBuilder()
                .WithColor(Global.RandomColorFromPalette())
                .WithTitle(serverDetails.Name)
                .WithDescription($"You can get the full playerlist by running: `{(await _guildService.GetPrefix(Context.Guild.Id))}players {ip}`")
                .WithFooter($"Query took: {queryTime}ms");

            embed.AddField("Map:", $"`{serverDetails.Map}`", true);
            embed.AddField("Players:", $"{serverDetails.PlayerCount}/{serverDetails.MaxPlayers}", true);
            embed.AddField("Gamemode:", serverDetails.Game, true);
            embed.AddField("Host OS:", $"{serverDetails.OS.ToString().ToTitleCase()} ({serverDetails.Dedicated.ToString().ToTitleCase()})", true);
            embed.AddField("VAC Status:", serverDetails.VAC, true);

            await ReplyAsync(embed: embed.Build());
        }
    }
}
