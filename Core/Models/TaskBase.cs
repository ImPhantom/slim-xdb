﻿using System;
using Newtonsoft.Json;
using System.Net.Http;
using System.Collections.Generic;
using System.Net;

namespace sXDB.Core.Models
{
    public class QueueTask
    {
        [JsonProperty("id")]
        public int TaskId { get; set; }

        [JsonProperty("guild_id")]
        public ulong GuildId { get; set; } = ulong.MinValue;

        [JsonProperty("task_type")]
        public string TaskType { get; set; }

        [JsonProperty("task_data")]
        public string TaskData { get; set; }

        [JsonProperty("execute_time")]
        public DateTime ExecuteTime { get; set; } = DateTime.MinValue;
    }

    public class PrivateMessageTaskData
    {
        [JsonProperty("user_id")]
        public ulong UserId { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("embed")]
        public bool Embed { get; set; }

        [JsonProperty("require_staff_ranks")]
        public bool RequireStaff { get; set; } = false;
    }

    public class MessageGuildTaskData
    {
        [JsonProperty("channel_name")]
        public string ChannelName { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("embed")]
        public bool Embed { get; set; }
    }

    public class ApiResponse
    {
        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("response")]
        public string Response { get; set; }
    }

    public enum TaskResult
    {
        Handled = 4,
        Unhandled = 3,
        SoftFailure = 2,
        CriticalFailure = 1
    }

    public class TaskResponse
    {
        public List<Result> Results { get; set; }

        public ApiResponse Response { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class Result
    {
        public int TaskId { get; set; }
        public TaskResult ResultValue { get; set; }
    }
}
