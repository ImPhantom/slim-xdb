﻿using Newtonsoft.Json;

namespace sXDB.Core.Models
{
    public class StatisticsResponse
    {
        [JsonProperty("state")]
        public string State { get; set; } = null;

        [JsonProperty("response")]
        public string ErrorMessage { get; set; } = null;

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }
        
        [JsonProperty("avatar_url")]
        public string AvatarUrl { get; set; }

        [JsonProperty("user_steamid")]
        public string SteamId { get; set; }

        [JsonProperty("tickets")]
        public StatisticSpan Tickets { get; set; }
        [JsonProperty("sessions")]
        public StatisticSpan Sessions { get; set; }
    }

    public class StatisticSpan
    {
        [JsonProperty("current_week")]
        public string CurrentWeek { get; set; }
        [JsonProperty("last_week")]
        public string LastWeek { get; set; }
        [JsonProperty("total")]
        public string AllTime { get; set; }
    }
}
