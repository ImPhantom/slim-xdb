﻿using System.Collections.Generic;

namespace sXDB.Core.Models
{
    public class QuickReplies
    {
        public ulong GuildId { get; set; }
        public List<QuickReply> Replies { get; set; }
    }

    public class QuickReply
    {
        public string ReplyName { get; set; }
        public List<ulong> AllowedChannels { get; set; } = new List<ulong>();
        public List<string> Triggers { get; set; }
        public string Response { get; set; }
    }

    public class QuickReplyEditResult
    {
        public Dictionary<ulong, bool> ModifiedChannels { get; set; } = new Dictionary<ulong, bool>();
        public Dictionary<string, bool> ModifiedTriggers { get; set; } = new Dictionary<string, bool>();

        public bool WasDeleted { get; set; } = false;
        public bool ResponseModified { get; set; } = false;
    }
}
