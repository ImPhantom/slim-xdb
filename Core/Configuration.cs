﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace sXDB.Core
{
    public class Configuration
    {
        // Serialized Values
        public string DefaultPrefix { get; set; } = ">>";
        public string BotToken { get; set; } = "token_goes_here";
        
        public string XenoApiToken { get; set; }

        [JsonIgnore]
        private readonly string FilePath = Path.Combine(AppContext.BaseDirectory, "config/base.json");

        public Task Initialize()
        {
            var dir = Path.Combine(AppContext.BaseDirectory, "config");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            // If config doesnt exist
            if (!File.Exists(FilePath))
            {
                var _config = new Configuration();
                _config.Save();

                Console.WriteLine("A config file has been created at: \"config/base.json\"\r\nOpen that file and put your bot token where it says to.\r\n\r\nPress any key to exit the application.");
                Console.ReadKey();
                return Task.CompletedTask;
            }

            return Task.CompletedTask;
        }

        public Task<Configuration> Load()
        {
            var config = JsonConvert.DeserializeObject<Configuration>(File.ReadAllText(FilePath));
            return Task.FromResult(config);
        }

        public Task Save()
        {
            var json = JsonConvert.SerializeObject(this, Formatting.Indented);
            File.WriteAllText(FilePath, json);
            return Task.CompletedTask;
        }
    }
}
