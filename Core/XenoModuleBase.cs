﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace sXDB.Core
{
    public class XenoModuleBase : ModuleBase<SocketCommandContext>
    {
        public async Task ReplyOkAsync()
            => await Context.Message.AddReactionAsync(new Emoji("👌"));

        public async Task<IUserMessage> ReplyThenRemoveAsync(string message, TimeSpan? timeout = null)
        {
            timeout = timeout ?? TimeSpan.FromSeconds(6);
            var reply = await base.ReplyAsync(message).ConfigureAwait(false);
            _ = Task.Delay(timeout.Value).ContinueWith(_ => reply.DeleteAsync().ConfigureAwait(false)).ConfigureAwait(false);
            return reply;
        }

        public async Task<IUserMessage> ReplyErrorAsync(string errorMessage, string title = null)
        {
            var embed = new EmbedBuilder()
                .WithColor(Global.ErrorColor)
                .WithTitle((title != null) ? title : "An error has occurred:")
                .WithDescription(errorMessage);

            return await base.ReplyAsync(embed: embed.Build());
        }

        public async Task<IUserMessage> ReplyNoticeAsync(string message, string title = null)
        {
            var embed = new EmbedBuilder()
                .WithColor(Global.RandomColorFromPalette())
                .WithTitle((title != null) ? title : "Notice:")
                .WithDescription(message);

            return await base.ReplyAsync(embed: embed.Build());
        }

        public async Task<IUserMessage> ReplyPrettyEmbedAsync(string message, string title = "Management Portal", string footer = "")
        {
            var embed = new EmbedBuilder()
                .WithAuthor(
                    title,
                    "https://xeno.gg/resources/media/mp_flat_out_logo.png"
                )
                .WithDescription(message)
                .WithColor(Global.RandomColorFromPalette())
                .WithFooter(footer);

            return await base.ReplyAsync(embed: embed.Build());
        }

        public async Task<IUserMessage> ReplyPrettyEmbedAsync(string message, DateTimeOffset timestamp, string title = "Management Portal")
        {
            var embed = new EmbedBuilder()
                .WithAuthor(
                    title,
                    "https://xeno.gg/resources/media/mp_flat_out_logo.png"
                )
                .WithDescription(message)
                .WithColor(Global.RandomColorFromPalette())
                .WithTimestamp(timestamp);

            return await base.ReplyAsync(embed: embed.Build());
        }
    }
}
