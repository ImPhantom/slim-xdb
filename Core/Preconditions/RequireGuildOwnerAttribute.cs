﻿using System;
using System.Threading.Tasks;
using Discord.Commands;

namespace sXDB.Core.Preconditions
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    class RequireGuildOwnerAttribute : PreconditionAttribute
    {
        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider serviceProvider)
        {
            var guildOwnerId = context.Guild.OwnerId;

            if (context.User.Id == guildOwnerId)
                return Task.FromResult(PreconditionResult.FromSuccess());
            else
                return Task.FromResult(PreconditionResult.FromError($"`{command.Name}` can only be run by the guilds owner."));
        }
    }
}
