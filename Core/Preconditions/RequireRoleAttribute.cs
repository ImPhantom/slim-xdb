﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord.WebSocket;
using Discord.Commands;

namespace sXDB.Core.Preconditions
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class RequireRoleAttribute : PreconditionAttribute
    {
        private string[] _requiredRoles;
        public RequireRoleAttribute(params string[] _roles)
            => _requiredRoles = _roles;

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider serviceProvider)
        {
            var execUser = context.User as SocketGuildUser;

            if (execUser.Roles.Any(role => _requiredRoles.Contains(role.Name)))
                return Task.FromResult(PreconditionResult.FromSuccess());
            else
                return Task.FromResult(PreconditionResult.FromError($"`{command.Name}` requires you to have one of the following roles:\r\n```{string.Join(", ", _requiredRoles)}```"));
        }
    }
}
