﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Discord;
using Discord.Rest;
using Discord.WebSocket;
using sXDB.Services;
using sXDB.Core.Extensions;
using Humanizer;

namespace sXDB.Core
{
    public class EventHandler
    {
        private readonly DiscordSocketClient _client;
        private readonly LoggingService _logger;
        private readonly GuildService _guildService;
        private readonly ModerationService _modService;

        public EventHandler(DiscordSocketClient client, LoggingService logger, GuildService guildService, ModerationService modService)
        {
            _client = client;
            _logger = logger;
            _guildService = guildService;
            _modService = modService;
        }

        public Task RegisterEvents()
        {
            _client.Ready += OnConnectionReady;

            _client.JoinedGuild += OnGuildJoin;
            _client.LeftGuild += OnGuildLeave;
            _client.GuildAvailable += OnGuildAvailable;

            _client.UserJoined += OnUserJoin;
            _client.UserLeft += OnUserLeave;

            _client.UserUnbanned += OnUserUnbanned;

            _client.MessageUpdated += OnMessageModify;
            _client.MessageDeleted += OnMessageDelete;
            _client.MessagesBulkDeleted += OnMessageBulkDelete;

            /* TODO: User Like/Reputation System?
             *  Listen on ReactionAdded for a certain reaction
             *  Increase a 'likes' column in a 'UserStats' table
             */

            return Task.CompletedTask;
        }

        private async Task OnConnectionReady()
        {
            var guilds = _client.Guilds.Count;
            await _logger.LogAsync(LogSeverity.Info, "Events", $"Bot is now active in {guilds} guilds.");
        }

        private async Task OnGuildJoin(SocketGuild guild)
        {
            if(!_guildService.HasGuildConfig(guild.Id))
            {
                // create a base guild config
                await _guildService.CreateBaseGuildConfig(guild.Id);

                // send the guild owner an introduction to the bot
                var application = await _client.GetApplicationInfoAsync();
                var guildOwner = await guild.Owner.GetOrCreateDMChannelAsync();
                await guildOwner.SendMessageAsync($@"
Source Code: <https://gitlab.com/ImPhantom/slim-xdb>
sXDB Wiki: https://gitlab.com/ImPhantom/slim-xdb/wikis/Home
If you have any issues please direct message: {application.Owner.Mention} (`{application.Owner}`)

**My default prefix is:** `>>` (to change this run: `>>set prefix !`)

:exclamation: **Check the wiki for important setup information!**");

                await _logger.LogAsync(LogSeverity.Info, "EventHandler", $"Joined {guild.Name}. (Introduction distributed to guild owner)");
            }
            else
                await _logger.LogAsync(LogSeverity.Info, "EventHandler", $"Joined {guild.Name}. (Guild is already configured, no introduction sent)");
        }

        private async Task OnGuildLeave(SocketGuild guild)
        {
            await _logger.LogAsync(LogSeverity.Info, "EventHandler", $"Left Guild! ({guild.Name})");
            // TODO: remove data for guild from database?
        }

        private async Task OnGuildAvailable(SocketGuild guild)
        {
            await _guildService.EnsureGuildsValidity(guild);
        }

        private async Task OnUserJoin(SocketGuildUser user)
        {
            if (_modService.IsUserMuted(user.Guild.Id, user.Id))
            {
                var mutedRole = _guildService.GetMutedRole(user.Guild.Id);
                await user.AddRoleAsync(mutedRole);
            }

            var guildLogChannel = await _guildService.GetLoggingChannel(user.Guild.Id);
            await guildLogChannel?.SendMessageAsync($":white_check_mark: {user.Mention} has joined the guild.");
        }

        private async Task OnUserLeave(SocketGuildUser user)
        {
            var guildBans = await user.Guild.GetBansAsync();
            if (guildBans.Any(_ban => _ban.User.Id == user.Id))
                return;

            var guildLogChannel = await _guildService.GetLoggingChannel(user.Guild.Id);
            await guildLogChannel?.SendMessageAsync($":x: {user.Mention} has left the guild.");
        }

        private async Task OnUserUnbanned(SocketUser user, SocketGuild guild)
        {
            await _modService.TerminateUsersTemporaryBans(guild.Id, user.Id);
        }

        private async Task OnMessageModify(Cacheable<IMessage, ulong> messageBefore, SocketMessage messageAfter, ISocketMessageChannel channel)
        {
            // TODO: check if messageBefore contained a mention
            if(channel is ITextChannel)
            {
                var logChannel = await (channel as SocketTextChannel).Guild.GetLoggingChannel();

                var before = (messageBefore.HasValue ? messageBefore.Value : null) as SocketUserMessage;
                var after = messageAfter as SocketUserMessage;

                if (after.Author.IsBot)
                    return;

                if (messageAfter.ContainsSpoilers())
                    await messageAfter.DeleteAsync();

                if (after.Content == null || before == null || before.Content == after.Content)
                    return;

                if(logChannel != null)
                    await logChannel.SendMessageAsync($":heavy_division_sign: `{after.Author}` has edited their message in {(channel as SocketTextChannel).Mention}:\r\n**Before:** {before.Content.Replace("||", "\\||")}\r\n**After:** {after.Content.Replace("||", "\\||")}");
            }
        }

        private async Task OnMessageDelete(Cacheable<IMessage, ulong> _message, ISocketMessageChannel channel)
        {
            // TODO: check if message contains a mention
            if(channel is ITextChannel)
            {
                var logChannel = await (channel as SocketTextChannel).Guild.GetLoggingChannel();
                var message = await _message.GetOrDownloadAsync();

                // obtain a list of attachment urls
                List<string> attachments = new List<string>();
                if (message.Attachments.Any())
                    foreach (var attachment in message.Attachments)
                        attachments.Add($"({attachment.Filename}) {attachment.Url}");

                if (message.Author.IsBot)
                    return;

                // obtain the user in which the message was deleted by (if any).
                var deletedBy = message.Author;
                var auditLog = (await (channel as ITextChannel).Guild.GetAuditLogsAsync(1)).ToList()[0];
                var auditData = auditLog.Data as MessageDeleteAuditLogData;

                if (auditData?.ChannelId == channel.Id && auditLog.Action == ActionType.MessageDeleted)
                        if (!auditLog.User.IsBot)
                            deletedBy = auditLog.User;

                string logMessage;
                var channelMention = (channel as SocketTextChannel).Mention;

                // if message contains user mentions, and the message was deleted by its author.
                // possible ghost ping
                if (message.MentionedUserIds.Any() && message.Author.Id == deletedBy.Id)
                    logMessage = $":ghost: Possible ghost ping by {message.Author.Mention} in {channelMention}:\r\n{message.Content.Replace("||", "\\||")}\r\n{string.Join("\r\n", attachments)}\r\n**Message Age:** `{(DateTimeOffset.Now - message.CreatedAt).Humanize()}`";
                else if (message.Author.Id == deletedBy.Id) // if message didnt have mentions, but was just deleted by the author
                    logMessage = $":heavy_minus_sign: {message.Author.Mention} has deleted their message in {channelMention}:\r\n{message.Content.Replace("||", "\\||")}\r\n{string.Join("\r\n", attachments)}";
                else // no mentions, deleted by someone other than the author
                    logMessage = $":heavy_minus_sign: {deletedBy.Mention} has deleted {message.Author.Mention}'s message in {channelMention}:\r\n{message.Content.Replace("||", "\\||")}\r\n{string.Join("\r\n", attachments)}";

                if (logChannel != null)
                    await logChannel.SendMessageAsync(logMessage);
            }
        }

        private async Task OnMessageBulkDelete(IReadOnlyCollection<Cacheable<IMessage, ulong>> messagesDeleted, ISocketMessageChannel channel)
        {
            if (channel is ITextChannel)
            {
                var logChannel = await (channel as SocketTextChannel).Guild.GetLoggingChannel();
                if (logChannel != null)
                    await logChannel.SendMessageAsync($":heavy_minus_sign: {messagesDeleted.Count()} messages were bulk deleted in #{channel.Name}.");
            }   
        }
    }
}
