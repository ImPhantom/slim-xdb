﻿using Discord.WebSocket;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace sXDB.Core.Extensions
{
    public static class MessageExtensions
    {
        public static bool ContainsSpoilers(this SocketMessage message)
        {
            if (!string.IsNullOrEmpty(message.Content))
                if (Regex.Matches(message.Content, "\\|\\|").Count >= 2)
                    return true;

            if (message.Attachments.Any())
                if (message.Attachments.Any(_attachment => _attachment.Filename.StartsWith("SPOILER_")))
                    return true;

            return false;
        }

        // this belongs in like a "StringExtensions" class butt fuck it
        public static string ToTitleCase(this string str)
            => CultureInfo.InvariantCulture.TextInfo.ToTitleCase(str.ToLower());
    }
}
