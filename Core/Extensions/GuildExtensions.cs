﻿using System.Linq;
using System.Threading.Tasks;
using Discord.WebSocket;
using sXDB.Database;
using sXDB.Database.Models;

namespace sXDB.Core.Extensions
{
    public static class GuildExtensions
    {
        public static Task<Guild> GetGuildConfig(this SocketGuild guild)
        {
            using (var db = new DatabaseContext())
            {
                if (!db.Guilds.Any(_guild => _guild.Id == guild.Id))
                    return null;

                var config = db.Guilds.First(_guild => _guild.Id == guild.Id);

                return Task.FromResult(config);
            }
        }

        public static string GetPrefix(this SocketGuild guild)
        {
            using (var db = new DatabaseContext())
            {
                if (!db.Guilds.Any(_guild => _guild.Id == guild.Id))
                    return null;

                var config = db.Guilds.First(_guild => _guild.Id == guild.Id);

                return config.Prefix;
            }
        }

        public static async Task<SocketTextChannel> GetLoggingChannel(this SocketGuild guild)
        {
            var guildConfig = await guild.GetGuildConfig();
            if (guildConfig == null || !guildConfig.LoggingChannel.HasValue)
                return null;

            return guild.GetTextChannel(guildConfig.LoggingChannel.Value);
        }
    }
}
