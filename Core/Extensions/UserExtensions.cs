﻿using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;

namespace sXDB.Core.Extensions
{
    public static class UserExtensions
    {
        public static bool HasStaffRoles(this SocketGuildUser guildUser)
        {
            var roles = guildUser.Roles;
            if (roles is IReadOnlyCollection<SocketRole>)
                if (roles.Any(_role => Global.StaffRoles.Contains(_role.Name)))
                    return true;

            return false;
        }
    }
}
