﻿using System.Linq;
using System.Threading.Tasks;
using Discord.WebSocket;
using sXDB.Database;
using Discord;

namespace sXDB.Core.Extensions
{
    public static class ChannelExtensions
    {
        public async static Task<IUserMessage> SendEmbedAsync(this ISocketMessageChannel channel, string description, string title = "", string footer = "", bool currentTimestamp = false)
        {
            var embed = new EmbedBuilder()
                .WithDescription(description)
                .WithColor(Global.RandomColorFromPalette());

            if (title != string.Empty)
                embed.Title = title;

            if (footer != string.Empty && !currentTimestamp)
                embed.Footer = new EmbedFooterBuilder().WithText(footer);

            if (footer == string.Empty || footer == null && currentTimestamp)
                embed = embed.WithCurrentTimestamp();

            return await channel.SendMessageAsync(embed: embed.Build());
        }

        public async static Task<IUserMessage> SendErrorAsync(this ISocketMessageChannel channel, string description, string title = "")
        {
            var embed = new EmbedBuilder()
                .WithDescription(description)
                .WithColor(Global.ErrorColor)
                .WithCurrentTimestamp();

            if (title != string.Empty)
                embed.Title = title;

            return await channel.SendMessageAsync(embed: embed.Build());
        }

        public static Task<bool> IsBlacklisted(this SocketTextChannel textChannel)
        {
            using (var db = new DatabaseContext())
            {
                var guildConfig = db.Guilds.First(_guild => _guild.Id == textChannel.Guild.Id);

                if(!string.IsNullOrEmpty(guildConfig.BlacklistedChannels))
                    if (guildConfig.BlacklistedChannels.Contains(textChannel.Id.ToString()))
                        return Task.FromResult(true);
            }

            return Task.FromResult(false);
        }
    }
}
