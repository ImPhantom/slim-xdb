﻿using System.ComponentModel.DataAnnotations;

namespace sXDB.Database.Models
{
    public class Guild
    {
        [Key]
        public ulong Id { get; set; }
        public string Prefix { get; set; }

        public string BlacklistedChannels { get; set; }
        public ulong? LoggingChannel { get; set; }
        public ulong? MutedRoleId { get; set; }
    }
}
