﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sXDB.Database.Models
{
    public class Mute
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MuteId { get; set; }

        public ulong GuildId { get; set; }

        public ulong MutedUserId { get; set; }
        public ulong MutingUserId { get; set; }

        public string Reason { get; set; }
        public DateTime DateMuted { get; set; }
        public DateTime? DateUnmuted { get; set; }
    }
}
