﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sXDB.Database.Models
{
    public class Tag
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TagId { get; set; }

        public ulong GuildId { get; set; }

        public ulong CreatorId { get; set; }
        public DateTime CreationDate { get; set; }

        public string Name { get; set; }
        public string Content { get; set; }
    }
}
