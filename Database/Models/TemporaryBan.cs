﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sXDB.Database.Models
{
    public class TemporaryBan
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BanId { get; set; }

        public ulong GuildId { get; set; }

        public ulong BannedUserId { get; set; }
        public string BannedUserTag { get; set; }

        public ulong BanningUserId { get; set; }

        public string Reason { get; set; }
        public DateTime DateBanned { get; set; }
        public DateTime DateUnbanned { get; set; }
    }
}
