﻿using Microsoft.EntityFrameworkCore;
using sXDB.Database.Models;
using System;
using System.IO;

namespace sXDB.Database
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Guild> Guilds { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TemporaryBan> TemporaryBans { get; set; }
        public DbSet<Mute> Mutes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Guild>(guild =>
            {
                guild.Property(_guild => _guild.LoggingChannel).IsRequired(false);
                guild.Property(_guild => _guild.MutedRoleId).IsRequired(false);
            });

            modelBuilder.Entity<Tag>()
                .Property(tag => tag.TagId)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<TemporaryBan>(ban =>
            {
                ban.Property(_ban => _ban.BanId).ValueGeneratedOnAdd();
                ban.Property(_ban => _ban.Reason).IsRequired(false);
            });

            modelBuilder.Entity<Mute>(mute =>
            {
                mute.Property(_mute => _mute.MuteId).ValueGeneratedOnAdd();
                mute.Property(_mute => _mute.DateUnmuted).IsRequired(false);
            });
                
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var dirPath = Path.Combine(AppContext.BaseDirectory, "data");
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);


            var path = Path.Combine(AppContext.BaseDirectory, "data/core.db");
            optionsBuilder.UseSqlite($"Data Source={path};");
        }
    }


}
