﻿using System;

namespace sXDB.SourceQuery
{
    public class SourceQueryException : Exception
    {
        public SourceQueryException(string message) : base(message) { }
    }
}
