﻿using System.Linq;
using System.Threading.Tasks;
using Discord.WebSocket;
using sXDB.Core;
using sXDB.Database;
using sXDB.Database.Models;
using System.Collections.Generic;

namespace sXDB.Services
{
    public class GuildService
    {
        private readonly DiscordSocketClient _client;
        private readonly LoggingService _logger;

        public GuildService(DiscordSocketClient client, LoggingService logger)
        {
            _client = client;
            _logger = logger;
        }

        public async Task EnsureGuildsValidity(SocketGuild guild)
        {
            using (var db = new DatabaseContext())
            {
                if (db.Guilds.Any(_guild => _guild.Id == guild.Id))
                    return;

                await _logger.LogAsync(Discord.LogSeverity.Info, "Guild Service", $"{guild.Name}'s guild config was missing, creating default config...");
                await CreateBaseGuildConfig(guild.Id);
            }
        }

        public bool HasGuildConfig(ulong guildId)
        {
            using (var db = new DatabaseContext())
                if (db.Guilds.Any(guild => guild.Id == guildId))
                    return true;

            return false;
        }

        public async Task CreateBaseGuildConfig(ulong guildId)
        {
            var coreConfig = await (new Configuration()).Load();
            var guildObject = new Guild()
            {
                Id = guildId,
                Prefix = coreConfig.DefaultPrefix
            };
            await SaveGuildConfigAsync(guildObject);
        }

        public async Task<Guild> GetGuildConfig(ulong guildId)
        {
            if (!HasGuildConfig(guildId))
                await CreateBaseGuildConfig(guildId);

            using (var db = new DatabaseContext())
                return db.Guilds.First(_guild => _guild.Id == guildId);
        }

        public async Task SaveGuildConfigAsync(Guild guildObject)
        {
            using (var db = new DatabaseContext())
            {
                if (db.Guilds.Any(_guild => _guild.Id == guildObject.Id))
                    db.Guilds.Update(guildObject);
                else
                    db.Guilds.Add(guildObject);

                await db.SaveChangesAsync();
            }
        }

        public Task<string> GetPrefix(ulong guildId)
        {
            if (!HasGuildConfig(guildId))
                return Task.FromResult((new Configuration()).DefaultPrefix);

            using (var db = new DatabaseContext())
                return Task.FromResult(db.Guilds.FirstOrDefault(guild => guild.Id == guildId).Prefix);
        }

        public async Task SetPrefix(ulong guildId, string prefix)
        {
            using (var db = new DatabaseContext())
            {
                var guildConfig = db.Guilds.First(_guild => _guild.Id == guildId);
                guildConfig.Prefix = prefix;

                await db.SaveChangesAsync();
            }
        }

        public Task<SocketTextChannel> GetLoggingChannel(ulong guildId)
        {
            if (!HasGuildConfig(guildId))
                return null;

            using (var db = new DatabaseContext())
            {
                var channelId = db.Guilds.FirstOrDefault(_guild => _guild.Id == guildId).LoggingChannel;
                if (!channelId.HasValue)
                    return null;

                var guild = _client.GetGuild(guildId);
                return Task.FromResult(guild.GetTextChannel(channelId.Value));
            }
        }

        public async Task SetLoggingChannel(ulong guildId, ulong channelId)
        {
            using (var db = new DatabaseContext())
            {
                var guildConfig = db.Guilds.First(_guild => _guild.Id == guildId);
                guildConfig.LoggingChannel = channelId;

                await db.SaveChangesAsync();
            }
        }

        public SocketRole GetMutedRole(ulong guildId)
        {
            using (var db = new DatabaseContext())
            {
                var guildConfig = db.Guilds.First(_guild => _guild.Id == guildId);
                if (!guildConfig.MutedRoleId.HasValue)
                    return null;

                var guild = _client.GetGuild(guildId);
                var role = guild.GetRole(guildConfig.MutedRoleId.Value);

                return role;
            }
        }

        public async Task SetMuteRole(ulong guildId, ulong roleId)
        {
            using (var db = new DatabaseContext())
            {
                var guildConfig = db.Guilds.First(_guild => _guild.Id == guildId);
                guildConfig.MutedRoleId = roleId;

                await db.SaveChangesAsync();
            }
        }

        public Task<List<ulong>> GetBlacklistedChannels(ulong guildId)
        {
            using (var db = new DatabaseContext())
            {
                var guildConfig = db.Guilds.First(_guild => _guild.Id == guildId);
                if (guildConfig.BlacklistedChannels == string.Empty || guildConfig.BlacklistedChannels == null)
                    return null;

                var split = guildConfig.BlacklistedChannels.Split(',');
                var list = split.Where(_idStr => !string.IsNullOrEmpty(_idStr))
                                .Select(idStr => ulong.Parse(idStr.Replace(",", string.Empty))).ToList();

                return Task.FromResult(list);
            }
        }

        public async Task<bool> AddBlacklistedChannel(ulong guildId, ulong channelId)
        {
            using (var db = new DatabaseContext())
            {
                var guildConfig = db.Guilds.First(_guild => _guild.Id == guildId);

                var existingBlacklist = guildConfig.BlacklistedChannels;
                guildConfig.BlacklistedChannels = $"{existingBlacklist}{channelId},";

                await db.SaveChangesAsync();
                return true;
            }
        }

        public async Task<bool> RemoveBlacklistedChannel(ulong guildId, ulong channelId)
        {
            using (var db = new DatabaseContext())
            {
                var guildConfig = db.Guilds.First(_guild => _guild.Id == guildId);

                var existingBlacklist = guildConfig.BlacklistedChannels;
                if (!existingBlacklist.Contains($"{channelId},"))
                    return false;

                guildConfig.BlacklistedChannels = existingBlacklist.Replace($"{channelId},", string.Empty);

                await db.SaveChangesAsync();
                return true;
            }
        }
    }
}
