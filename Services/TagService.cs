﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using sXDB.Database;
using sXDB.Database.Models;

namespace sXDB.Services
{
    public class TagService
    {
        public Task<List<Tag>> GetTags(ulong guildId)
        {
            using (var db = new DatabaseContext())
                return Task.FromResult(db.Tags.Where(tag => tag.GuildId == guildId).ToList());
        }

        public async Task<Tag> GetTag(ulong guildId, string tagName)
        {
            var guildTags = await GetTags(guildId);
            if (!guildTags.Any(tag => tag.Name == tagName))
                return null;

            return guildTags.First(tag => tag.Name == tagName);
        }

        public async Task CreateTag(Tag tag)
        {
            using (var db = new DatabaseContext())
            {
                await db.Tags.AddAsync(tag);
                await db.SaveChangesAsync();
            }
        }

        public async Task<bool> RemoveTag(ulong guildId, string tagName)
        {
            using (var db = new DatabaseContext())
            {
                var guildTags = await GetTags(guildId);
                if (!guildTags.Any(_tag => _tag.Name == tagName))
                    return false;

                var tag = guildTags.First(_tag => _tag.Name == tagName);

                db.Remove(tag);
                await db.SaveChangesAsync();
                return true;
            }
        }
    }
}
