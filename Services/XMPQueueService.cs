﻿using Discord;
using Discord.WebSocket;
using Newtonsoft.Json;
using sXDB.Core;
using sXDB.Core.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Timers;
using System.Net.Http;
using System.Threading.Tasks;
using sXDB.Core.Extensions;

namespace sXDB.Services
{
    public class XMPQueueService
    {
        private readonly DiscordSocketClient _client;
        private readonly LoggingService _logger;
        private readonly TasksService _tasksService;
        private readonly GuildService _guildService;

        private string apiBase = "https://dev.xeno.gg/@";
        private string apiKey;

        private TaskResponse lastResponse;

        private ulong guildLock = ulong.MinValue;

        public XMPQueueService(DiscordSocketClient client, LoggingService logger, TasksService tasksService, GuildService guildService)
        {
            _client = client;
            _logger = logger;
            _tasksService = tasksService;
            _guildService = guildService;

            var config = new Configuration().Load().Result;

            apiKey = config.XenoApiToken;

            //guildLock = 93766273162285056; // development
            guildLock = 234453502879858692; // production
        }

        public async Task CreateQueueTasks()
        {
            await _tasksService.CreateNewTask("Queue::CoreTask", 12000, ProcessQueueTasks);
        }

        private async void ProcessQueueTasks(object sender, ElapsedEventArgs eventArgs)
        {
            var stopwatch = Stopwatch.StartNew();
            using (var httpClient = new HttpClient())
            {
                var taskStates = new List<Result>();
                var handledTasks = new List<int>(); // list to store handled task id's

                var tasksListResponse = await httpClient.GetAsync($"{apiBase}/queue/fetch/?token={apiKey}");
                if(tasksListResponse.IsSuccessStatusCode)
                {   
                    var responseAsJson = await tasksListResponse.Content.ReadAsStringAsync();
                    try
                    {
                        var queue = JsonConvert.DeserializeObject<List<QueueTask>>(responseAsJson);
                        var taskResult = TaskResult.Unhandled;
                        foreach (var task in queue)
                        {
                            var realResponseTime = (task.ExecuteTime != DateTime.MinValue) ? ((DateTime.UtcNow - task.ExecuteTime).TotalSeconds) + stopwatch.Elapsed.TotalSeconds : stopwatch.Elapsed.TotalSeconds; // stopwatch fallback

                            var responseTime = Math.Round(realResponseTime, 3);
                            switch (task.TaskType)
                            {
                                case "private_message_user":
                                    var pmData = JsonConvert.DeserializeObject<PrivateMessageTaskData>(task.TaskData);
                                    taskResult = await HandlePrivateMessageTask(task, responseTime);
                                    break;
                                case "message_guild_channel":
                                    var mgData = JsonConvert.DeserializeObject<MessageGuildTaskData>(task.TaskData);
                                    taskResult = await HandleMessageGuildTask(task, responseTime);
                                    break;
                            }

                            if (taskResult == TaskResult.Handled || taskResult == TaskResult.CriticalFailure)
                                handledTasks.Add(task.TaskId); // consider task handled & not to be tried again.

                            taskStates.Add(new Result()
                            {
                                TaskId = task.TaskId,
                                ResultValue = taskResult
                            });
                        }
                    } catch(Exception exception)
                    {
                        await _logger.LogToFileAsync("process_queue_fail", $"{exception.Message}\r\n{exception.ToString()}\r\n\r\n{responseAsJson}", "Queue::CoreTask failed to execute properly.");
                        return;
                    }             

                    if(handledTasks.Count > 0)
                        await SendSuccessRequest(handledTasks, taskStates);

                    if (taskStates.Count > handledTasks.Count())
                        await _logger.LogAsync(LogSeverity.Info, "Task Queue", $"{taskStates.Count - handledTasks.Count()} tasks have failed to execute, waiting for next service tick...");

                }
            }
        }

        private async Task<ApiResponse> SendSuccessRequest(List<int> handledTasks, List<Result> taskStates)
        {
            using (var httpClient = new HttpClient())
            {
                var requestData = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("token", apiKey),
                    new KeyValuePair<string, string>("handled-tasks", string.Join(',', handledTasks))
                });

                var successResponse = await httpClient.PostAsync($"{apiBase}/queue/update/", requestData);
                if(successResponse.IsSuccessStatusCode)
                {
                    var responseString = await successResponse.Content.ReadAsStringAsync();

                    await _logger.LogAsync(LogSeverity.Info, "Task Queue", $"Successfully handled {handledTasks.Count()} tasks. Response:");
                    await _logger.LogAsync(LogSeverity.Info, "Task Queue", responseString);

                    var response = JsonConvert.DeserializeObject<ApiResponse>(responseString);

                    lastResponse = new TaskResponse()
                    {
                        Results = taskStates,
                        Response = response,
                        StatusCode = successResponse.StatusCode,
                        Timestamp = DateTime.UtcNow
                    };

                    return response;
                }
            }

            return null;
        }

        private async Task<TaskResult> HandlePrivateMessageTask(QueueTask _task, double responseTime)
        {
            var taskData = JsonConvert.DeserializeObject<PrivateMessageTaskData>(_task.TaskData);
            var guildId = (guildLock != ulong.MinValue) ? guildLock : ((_task.GuildId != ulong.MinValue) ? _task.GuildId : ulong.MinValue);

            try
            {
                var user = _client.GetUser(taskData.UserId) as SocketGuildUser;
                if (guildId != ulong.MinValue)
                {
                    var guild = _client.GetGuild(guildId);
                    user = guild.GetUser(taskData.UserId);
                }

                if (taskData.RequireStaff) // if task requires staff roles, and a guild is supplied
                    if (!user.HasStaffRoles()) // if the user doesnt have staff roles
                        return TaskResult.CriticalFailure; // dont send

                var channel = await user.GetOrCreateDMChannelAsync();
                await SendChannelEmbedMessage(channel, taskData.Content, responseTime);

                return TaskResult.Handled;
            }
            catch (Exception exception)
            {
                await _logger.LogAsync(LogSeverity.Error, "Task Queue", $"Failed to private message user: {guildId}/{taskData.UserId}");
                await _logger.LogAsync(LogSeverity.Error, "Task Queue", $"{exception.ToString()}");
                return TaskResult.CriticalFailure;
            }    
        }

        private async Task<TaskResult> HandleMessageGuildTask(QueueTask _task, double responseTime)
        {
            var taskData = JsonConvert.DeserializeObject<MessageGuildTaskData>(_task.TaskData);
            var guildId = (guildLock != ulong.MinValue) ? guildLock : ((_task.GuildId != ulong.MinValue) ? _task.GuildId : ulong.MinValue);

            if (guildId == ulong.MinValue)
                return TaskResult.CriticalFailure; // this requires a guild

            try
            {
                var guild = _client.GetGuild(guildId);
                if (!guild.TextChannels.Any(_channel => _channel.Name.ToLower() == _channel.Name.ToLower()))
                    return TaskResult.CriticalFailure; // no text channels found

                var channel = guild.TextChannels.First(_channel => _channel.Name.ToLower() == taskData.ChannelName.ToLower());

                if (taskData.Embed)
                    await SendChannelEmbedMessage(channel, taskData.Content, responseTime);
                else
                    await channel.SendMessageAsync(taskData.Content);

                return TaskResult.Handled;

            } catch (Exception exception)
            {
                await _logger.LogAsync(LogSeverity.Error, "Task Queue", $"Failed to message guild channel (guildId/channelName): ({_task.GuildId}/{taskData.ChannelName})");
                await _logger.LogAsync(LogSeverity.Error, "Task Queue", $"{exception.ToString()}");
                return TaskResult.CriticalFailure;
            }
        }

        private async Task SendChannelEmbedMessage(IMessageChannel channel, string content, double responseTime)
        {
            var embed = new EmbedBuilder()
                .WithAuthor(
                    "Management Portal",
                    "https://xeno.gg/resources/media/mp_flat_out_logo.png"
                )
                .WithDescription(content)
                .WithColor(Global.RandomColorFromPalette())
                .WithFooter($"Request took: {responseTime}s");

            await channel.SendMessageAsync(embed: embed.Build());
        }

        public TaskResponse GetLastRequest() {
            return lastResponse;
        }
    }
}
