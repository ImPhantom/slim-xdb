﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Collections.Generic;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using sXDB.Core;
using sXDB.Core.Extensions;
using sXDB.Core.TypeReaders;
using sXDB.Core.Models;

namespace sXDB.Services
{
    public class CommandHandlingService
    {
        private readonly DiscordSocketClient _client;
        private readonly CommandService _commands;
        private readonly GuildService _guildService;
        private readonly ChatService _chatService;
        private IServiceProvider serviceProvider;

        public CommandHandlingService(IServiceProvider provider)
        {
            serviceProvider = provider;
            _client = serviceProvider.GetService(typeof(DiscordSocketClient)) as DiscordSocketClient;
            _commands = serviceProvider.GetService(typeof(CommandService)) as CommandService;
            _guildService = serviceProvider.GetService(typeof(GuildService)) as GuildService;
            _chatService = serviceProvider.GetService(typeof(ChatService)) as ChatService;
        }

        public async Task Install()
        {
            _commands.AddTypeReader<TimeSpan>(new TimeStringTypeReader(), true);
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), serviceProvider);
            _client.MessageReceived += HandleMessageReceived;
        }

        private async Task HandleMessageReceived(SocketMessage _message)
        {
            var message = _message as SocketUserMessage;
            if (message == null)
                return;

            var context = new SocketCommandContext(_client, message);
            var coreConfig = new Configuration();

            var allowedUsers = new List<ulong> { 97675548985143296, 93765631177920512 };
            var quickReplies = new Dictionary<string, string>
            {
                { "Who?", "Cares." },
                { "gg", "git gud" }
            };
            
            if(allowedUsers.Any(_id => _id == message.Author.Id))
            {
                if (quickReplies.Any(_quickReply => _quickReply.Key == message.Content))
                {
                    var quickReply = quickReplies.First(_quickReply => _quickReply.Key == message.Content);
                    var delay = (new Random()).Next(1800, 3200);

                    await context.Channel.TriggerTypingAsync();
                    _ = Task.Delay(delay).ContinueWith(_ => context.Channel.SendMessageAsync(quickReply.Value).ConfigureAwait(false)).ConfigureAwait(false);
                }
            }

            if (message.ContainsSpoilers() && !message.Author.IsBot)
                await message.DeleteAsync();

            bool userIsGuildBlacklisted = false;
            if(context.Guild != null)
                userIsGuildBlacklisted = await _chatService.IsUserBlacklisted(context.Guild.Id, message.Author.Id);

            if (context.Guild != null && !message.Author.IsBot && !userIsGuildBlacklisted)
                await _chatService.HandleTriggeredQuickReplies(context.Guild.Id, message.Channel as SocketTextChannel, message.Content);

            int argPos = 0;
            string prefix = coreConfig.DefaultPrefix;
            if(context.Guild != null)
                prefix = await _guildService.GetPrefix(context.Guild.Id);

            if (message.HasStringPrefix(prefix, ref argPos))
            {
                if (message.Author.IsBot)
                    return;

                // if user is blacklisted
                if (context.Guild != null && userIsGuildBlacklisted)
                    return;

                // if channel is blacklisted
                if (context.Guild != null && await (context.Channel as SocketTextChannel).IsBlacklisted())
                    if (!message.Content.Contains($"{prefix}tag"))
                        return;

                var result = await _commands.ExecuteAsync(context, argPos, serviceProvider);

                if (!result.IsSuccess)
                {
                    if(result.Error == CommandError.UnknownCommand)
                    {
                        List<string> suggestions = new List<string>();
                        foreach (var command in _commands.Commands)
                            if(command.Aliases.Any(_alias => _alias.Contains(context.Message.Content.Substring(argPos, 2))))
                                suggestions.Add($"`{prefix}{command.Aliases.First()}`");

                        if (suggestions.Count > 0)
                            await context.Channel.SendErrorAsync(string.Join("\r\n", suggestions), "Command not found, perhaps you meant:");
                    } else
                        await context.Channel.SendErrorAsync(result.ErrorReason, "Command failed to execute:");
                }
            }
        }
    }
}
