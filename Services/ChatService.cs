﻿using sXDB.Core.Models;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using Discord.WebSocket;

namespace sXDB.Services
{
    public class ChatService
    {
        private readonly DiscordSocketClient _client;
        private readonly GuildService _guilds;
        private readonly LoggingService _logger;
        private readonly StorageService _storage;

        private string repliesFile = "quick-replies";
        private string blacklistFile = "user-blacklist";
        private Dictionary<ulong, QuickReplies> RepliesCache = new Dictionary<ulong, QuickReplies>();

        public ChatService(DiscordSocketClient client, GuildService guilds, LoggingService logger, StorageService storage)
        {
            _client = client;
            _guilds = guilds;
            _logger = logger;
            _storage = storage;
        }

        public async Task<bool> IsUserBlacklisted(ulong guildId, ulong userId)
        {
            var dataFolder = await _storage.GetGuildDataFolder(guildId);
            if (await _storage.DataFileExists(blacklistFile, guildId: guildId))
            {
                var json = await _storage.GetJsonFromFile(blacklistFile, guildId: guildId);
                if(json != null)
                {
                    var data = JsonConvert.DeserializeObject<List<ulong>>(json);

                    if (data.Any(_id => _id == userId))
                        return true;
                    else
                        return false;
                }
            }

            return false;
        }

        public async Task<bool> BlacklistUser(ulong guildId, ulong userId)
        {
            var dataFolder = await _storage.GetGuildDataFolder(guildId);
            var data = new List<ulong>();
            if (await _storage.DataFileExists(blacklistFile, guildId: guildId))
            {
                var json = await _storage.GetJsonFromFile(blacklistFile, guildId: guildId);
                data = JsonConvert.DeserializeObject<List<ulong>>(json);
                if (data.Any(_id => _id == userId))
                    data.Remove(userId);
                else
                    data.Add(userId);
            }
            else
                data = new List<ulong>() { userId };

            await _storage.SaveJsonToFile(blacklistFile, data, guildId: guildId);
            return true;
        }

        public async Task<QuickReplies> GetQuickReplies(ulong guildId)
        {
            var dataFolder = await _storage.GetGuildDataFolder(guildId);
            if (await _storage.DataFileExists(repliesFile, guildId: guildId))
            {
                var jsonData = await _storage.GetJsonFromFile(repliesFile, guildId: guildId);
                return JsonConvert.DeserializeObject<QuickReplies>(jsonData);
            }

            return null;
        }

        public async Task<bool> HandleTriggeredQuickReplies(ulong guildId, SocketTextChannel channel, string messageContent)
        {
            var quickRepliesObject = await GetQuickReplies(guildId);

            if (quickRepliesObject != null && quickRepliesObject.Replies.Any(_reply => _reply.Triggers.Any(_trigger => messageContent.ToLower().Contains(_trigger.ToLower()))))
            {
                var reply = quickRepliesObject.Replies.First(_reply => _reply.Triggers.Any(_trigger => messageContent.ToLower().Contains(_trigger.ToLower())));
                if (!reply.AllowedChannels.Any() || reply.AllowedChannels.Any(_channelId => _channelId == channel.Id))
                {
                    var prefix = await _guilds.GetPrefix(guildId);
                    if (!messageContent.StartsWith($"{prefix}q"))
                    {
                        await channel.TriggerTypingAsync();
                        await Task.Delay((new Random()).Next(900, 1200)).ContinueWith(async _task => await channel.SendMessageAsync(reply.Response));
                        return true;
                    }

                    return false;
                }
            }
            return false;
        }

        public async Task<bool> AddQuickReply(ulong guildId, string replyName, string trigger, string response)
        {
            var quickReplies = await GetQuickReplies(guildId);
            if (quickReplies == null)
            {
                quickReplies = new QuickReplies()
                {
                    GuildId = guildId,
                    Replies = new List<QuickReply>()
                };
            }

            quickReplies.Replies.Add(new QuickReply()
            {
                ReplyName = replyName,
                Triggers = new List<string> { trigger },
                Response = response
            });

            await _storage.SaveJsonToFile(repliesFile, quickReplies, guildId: guildId);
            return true;
        }

        public async Task<bool> EditReplyResponse(ulong guildId, string replyName, string response = "")
        {
            var quickReplies = await GetQuickReplies(guildId);

            if (!quickReplies.Replies.Any(_reply => _reply.ReplyName == replyName))
                return false;

            var reply = quickReplies.Replies.First(_reply => _reply.ReplyName == replyName);
            quickReplies.Replies.Remove(reply);

            reply.Response = response;

            quickReplies.Replies.Add(reply);
            await _storage.SaveJsonToFile(repliesFile, quickReplies, guildId: guildId);
            return true;
        }

        public async Task<QuickReplyEditResult> EditReplyChannels(ulong guildId, string replyName, List<ulong> channelIds)
        {
            var quickReplies = await GetQuickReplies(guildId);

            if (!quickReplies.Replies.Any(_reply => _reply.ReplyName == replyName))
                return null;

            var reply = quickReplies.Replies.First(_reply => _reply.ReplyName == replyName);
            quickReplies.Replies.Remove(reply);
            var result = new QuickReplyEditResult();
            foreach (var channelId in channelIds)
            {
                if (reply.AllowedChannels.Any(_channelId => _channelId == channelId)) {
                    reply.AllowedChannels.Remove(channelId);
                    result.ModifiedChannels.Add(channelId, false);
                } else {
                    reply.AllowedChannels.Add(channelId);
                    result.ModifiedChannels.Add(channelId, true);
                }
            }

            quickReplies.Replies.Add(reply);
            await _storage.SaveJsonToFile(repliesFile, quickReplies, guildId: guildId);
            return result;
        }

        public async Task<QuickReplyEditResult> EditReplyTriggers(ulong guildId, string replyName, List<string> triggers)
        {
            var quickReplies = await GetQuickReplies(guildId);

            if (!quickReplies.Replies.Any(_reply => _reply.ReplyName == replyName))
                return null;

            var reply = quickReplies.Replies.First(_reply => _reply.ReplyName == replyName);
            quickReplies.Replies.Remove(reply);
            var result = new QuickReplyEditResult();
            foreach (var trigger in triggers)
            {
                if (reply.Triggers.Any(_trigger => _trigger == trigger)) {
                    reply.Triggers.Remove(trigger);
                    result.ModifiedTriggers.Add(trigger, false);
                } else {
                    reply.Triggers.Add(trigger);
                    result.ModifiedTriggers.Add(trigger, true);
                }
            }

            quickReplies.Replies.Add(reply);
            await _storage.SaveJsonToFile(repliesFile, quickReplies, guildId: guildId);
            return result;
        }

        public async Task<bool> RemoveQuickReply(ulong guildId, string replyName)
        {
            var quickReplies = await GetQuickReplies(guildId);

            if (!quickReplies.Replies.Any(_reply => _reply.ReplyName == replyName))
                return false;

            var reply = quickReplies.Replies.First(_reply => _reply.ReplyName == replyName);

            quickReplies.Replies.Remove(reply);
            await _storage.SaveJsonToFile(repliesFile, quickReplies, guildId: guildId);
            return true;
        }
    }
}
