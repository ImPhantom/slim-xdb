﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace sXDB.Services
{
    public class StorageService
    {
        private readonly LoggingService _logger;
        private readonly GuildService _guildService;

        private string jsonFilesDirectory = Path.Combine(AppContext.BaseDirectory, "data/json");

        public StorageService(LoggingService loggingService, GuildService guildService)
        {
            _logger = loggingService;
            _guildService = guildService;
        }

        public Task EnsureFolderExistence()
        {
            var dataDirectory = Path.Combine(AppContext.BaseDirectory, "data");
            if (!Directory.Exists(dataDirectory))
                Directory.CreateDirectory(dataDirectory);

            var jsonDirectory = Path.Combine(dataDirectory, "json");
            if (!Directory.Exists(jsonDirectory))
                Directory.CreateDirectory(jsonDirectory);

            return Task.CompletedTask;
        }

        public Task<string> GetGuildDataFolder(ulong guildId)
        {
            var dataFolder = Path.Combine(jsonFilesDirectory, $"{guildId}");
            if (!Directory.Exists(dataFolder))
                Directory.CreateDirectory(dataFolder);

            return Task.FromResult(dataFolder);
        }

        public async Task<bool> DataFileExists(string fileName, string guildDataFolder = "", ulong guildId = ulong.MinValue)
        {
            var dataFolder = (guildDataFolder != string.Empty) ? guildDataFolder : ((guildId != ulong.MinValue) ? await GetGuildDataFolder(guildId) : null);
            if(dataFolder != null)
            {
                var dataFile = Path.Combine(dataFolder, $"{fileName}.json");
                if (File.Exists(dataFile))
                    return true;
            }
            return false;
        }

        public async Task<bool> SaveJsonToFile(string filename, object rawObject = null, string jsonStr = "", string guildDataFolder = "", ulong guildId = ulong.MinValue)
        {
            var dataFolder = (guildDataFolder != string.Empty) ? guildDataFolder : ((guildId != ulong.MinValue) ? await GetGuildDataFolder(guildId) : null);

            if (dataFolder != null)
            {
                var jsonData = (rawObject != null) ? JsonConvert.SerializeObject(rawObject, Formatting.Indented) : ((jsonStr != string.Empty) ? jsonStr : null);
                if(jsonData != null)
                {
                    var filePath = Path.Combine(dataFolder, $"{filename}.json");
                    var encodedData = Encoding.Unicode.GetBytes(jsonData);
                    using (var fileStream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None, bufferSize: 4096, useAsync: true))
                    {
                        if(fileStream.Length > 0)
                            fileStream.SetLength(0);

                        await fileStream.WriteAsync(encodedData, 0, encodedData.Length);
                        return true;
                    }
                }
            }

            return false;
        }

        public async Task<string> GetJsonFromFile(string fileName, string guildDataFolder = "", ulong guildId = ulong.MinValue)
        {
            var dataFolder = (guildDataFolder != string.Empty) ? guildDataFolder : ((guildId != ulong.MinValue) ? await GetGuildDataFolder(guildId) : null);
            if(dataFolder != null)
            {
                var jsonFile = Path.Combine(dataFolder, $"{fileName}.json");
                if (File.Exists(jsonFile)) {
                    try
                    {
                        using (var fileStream = new FileStream(jsonFile, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize: 4096, useAsync: true))
                        {
                            var buffer = new byte[0x1000];
                            var stringBuffer = new StringBuilder();
                            int numRead;

                            while ((numRead = await fileStream.ReadAsync(buffer, 0, buffer.Length)) != 0)
                            {
                                string textFromBuffer = Encoding.Unicode.GetString(buffer, 0, numRead);
                                stringBuffer.Append(textFromBuffer);
                            }

                            return stringBuffer.ToString();
                        }
                    } catch (Exception exception)
                    {
                        await _logger.LogAsync(Discord.LogSeverity.Error, "Storage Service", "Failed reading json from {jsonFile}.");
                        await _logger.LogAsync(Discord.LogSeverity.Error, "Storage Service", exception.Message);
                        return null;
                    }
                }
            }
            return null;
        }
    }
}
