﻿using Discord;
using Discord.WebSocket;
using System.Linq;
using System.Timers;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace sXDB.Services
{
    public class TasksService
    {
        public DiscordSocketClient _client;
        public Dictionary<string, Timer> TaskTimers;

        private LoggingService _logger;

        public TasksService(DiscordSocketClient client, LoggingService logger)
        {
            _client = client;
            _logger = logger;
            TaskTimers = new Dictionary<string, Timer>();
        }

        public async Task CreateCoreTasks()
        {
            await CreateNewTask("Core::UpdateActivity", 48000, UpdateBotActivity);
        }

        public async Task<bool> CreateNewTask(string taskName, int timerInterval, ElapsedEventHandler eventHandler)
        {
            if (TaskTimers.ContainsKey(taskName.ToLower()))
                return false;

            var timer = new Timer()
            {
                Interval = timerInterval,
                AutoReset = true,
                Enabled = true
            };

            timer.Elapsed += eventHandler;
            TaskTimers.Add(taskName.ToLower(), timer);

            await _logger.LogAsync(LogSeverity.Info, "Task Service", $"Started running task: '{taskName}' (Tick: {(timerInterval / 1000)}s)");
            return true;
        }

        
        private async void UpdateBotActivity(object sender, ElapsedEventArgs eventArgs)
        {
            var users = _client.Guilds.Sum(_guild => _guild.MemberCount);
            await _client.SetGameAsync($"over {users} users.", type: Discord.ActivityType.Watching);
        }
    }
}
