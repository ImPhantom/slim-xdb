﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.EntityFrameworkCore;
using Discord.WebSocket;
using Humanizer;
using sXDB.Core.Extensions;
using sXDB.Database;
using sXDB.Database.Models;

namespace sXDB.Services
{
    public class ModerationService
    {
        private readonly DiscordSocketClient _client;
        private readonly LoggingService _logger;
        private readonly TasksService _tasksService;
        private readonly GuildService _guildService;

        public ModerationService(DiscordSocketClient client, LoggingService logger, TasksService tasksService, GuildService guildService)
        {
            _client = client;
            _logger = logger;
            _tasksService = tasksService;
            _guildService = guildService;
        }

        public async Task CreateModerationTasks()
        {
            await _tasksService.CreateNewTask("Mod::TerminateBans", 36000, TerminateExpiredTemporaryBans);
            await _tasksService.CreateNewTask("Mod::TerminateMutes", 24000, TerminateExpiredMutes);
        }

        // get active temp bans
        public List<TemporaryBan> GetActiveTemporaryBans(ulong guildId)
        {
            using (var db = new DatabaseContext())
            {
                if (!db.TemporaryBans.Any(_ban => _ban.GuildId == guildId))
                    return null;

                var guildBans = db.TemporaryBans.Where(_ban => _ban.GuildId == guildId);
                return guildBans.Where(_ban => _ban.DateUnbanned >= DateTime.UtcNow).ToList();
            }
        }

        // get temporary ban by id
        public TemporaryBan GetTemporaryBan(int banId)
        {
            using (var db = new DatabaseContext())
            {
                if (!db.TemporaryBans.Any(_ban => _ban.BanId == banId))
                    return null;

                return db.TemporaryBans.First(_ban => _ban.BanId == banId);
            }
        }

        // tempban user
        public async Task<TemporaryBan> TemporarilyBan(ulong guildId, ulong userId, ulong adminId, DateTime unbanDate, string reason = "")
        {
            var guild = _client.GetGuild(guildId);
            var user = _client.GetUser(userId);
            var admin = guild.GetUser(adminId);

            var ban = new TemporaryBan()
            {
                GuildId = guild.Id,
                BannedUserId = userId,
                BannedUserTag = $"{user.Username}#{user.Discriminator}",
                BanningUserId = adminId,
                Reason = reason,
                DateBanned = DateTime.UtcNow,
                DateUnbanned = unbanDate.ToUniversalTime()
            };

            using (var db = new DatabaseContext())
            {
                if (db.TemporaryBans.Any(_ban => _ban.BannedUserId == userId))
                    if ((db.TemporaryBans.First(_ban => _ban.BannedUserId == userId).DateUnbanned) > DateTime.UtcNow)
                        return null;

                await db.TemporaryBans.AddAsync(ban);
                await db.SaveChangesAsync();
            }

            var banReason = $"{reason} Temporarily banned by {admin.Username}#{admin.Discriminator}\r\n*This is a temporary ban, managed by {_client.CurrentUser.Username}*\r\n*Revoking this ban will terminate the temporary ban.*";
            await guild.AddBanAsync(userId, 0, banReason);
            return ban;
        }

        // remove users temp bans (used in onUserBanned event)
        public async Task TerminateUsersTemporaryBans(ulong guildId, ulong userId)
        {
            using (var db = new DatabaseContext())
            {
                if (!db.TemporaryBans.Any(_ban => _ban.GuildId == guildId))
                    return;

                var guildBans = db.TemporaryBans.Where(_ban => _ban.GuildId == guildId);
                if (guildBans.Any(_ban => _ban.BannedUserId == userId))
                {
                    var userBans = guildBans.Where(_ban => _ban.BannedUserId == userId).ToList(); // assuming user has multiple bans stored? why not.
                    foreach (var ban in userBans)
                        db.TemporaryBans.Remove(ban);

                    await db.SaveChangesAsync();
                }
            }
        }

        // terminate expired temp bans
        public async void TerminateExpiredTemporaryBans(object sender, ElapsedEventArgs eventArgs)
        {
            using (var db = new DatabaseContext())
            {
                var expiredBans = db.TemporaryBans.Where(_ban => _ban.DateUnbanned <= DateTime.UtcNow).ToList();

                if(expiredBans.Any())
                {
                    foreach (var ban in expiredBans)
                    {
                        var guild = _client.GetGuild(ban.GuildId);
                        var logChannel = await guild.GetLoggingChannel();
                        var guildBans = await guild.GetBansAsync();

                        // if a ban exists on the guild, remove it and log that we removed it.
                        if (guildBans.Any(_ban => _ban.User.Id == ban.BannedUserId))
                        {
                            var bannedUser = guildBans.First(_ban => _ban.User.Id == ban.BannedUserId).User;
                            await guild.RemoveBanAsync(ban.BannedUserId);
                            if(logChannel != null)
                                await logChannel.SendMessageAsync($"**{bannedUser.Username}#{bannedUser.Discriminator}**'s {(ban.DateUnbanned - ban.DateBanned).Humanize()} ban has expired!");
                        }
                        
                        // remove the ban from the db always.
                        db.Remove(ban);
                    }

                    await _logger.LogAsync(Discord.LogSeverity.Info, "Task::TemporaryBans", $"Task ran successfully, terminating {expiredBans.Count} expired temporary bans.");

                    try {
                        await db.SaveChangesAsync();
                    } catch (DbUpdateConcurrencyException exception) {
                        exception.Entries.ToList().ForEach(entry => entry.State = EntityState.Detached);
                    }
                }
            }
        }

        public bool HasActiveMutes(ulong guildId)
        {
            using (var db = new DatabaseContext())
            {
                if (db.Mutes.Any(_mute => _mute.GuildId == guildId))
                    return true;
                else
                    return false;
            }
        }

        // get active mutes
        public Task<List<Mute>> GetActiveMutes(ulong guildId)
        {
            using (var db = new DatabaseContext())
            {
                if (!db.Mutes.Any(_mute => _mute.GuildId == guildId))
                    return null;

                var guildMutes = db.Mutes.Where(_mute => _mute.GuildId == guildId);
                return Task.FromResult(guildMutes.ToList());
            }
        }

        // is user muted
        public bool IsUserMuted(ulong guildId, ulong userId)
        {
            using (var db = new DatabaseContext())
            {
                if (!db.Mutes.Any(_mute => _mute.GuildId == guildId))
                    return false;

                var guildMutes = db.Mutes.Where(_mute => _mute.GuildId == guildId);
                if (guildMutes.Any(_mute => _mute.MutedUserId == userId))
                    return true;

                return false;
            }
        }

        // mute user
        public async Task<bool> MuteUserAsync(ulong guildId, ulong userId, ulong adminId, string reason, TimeSpan? length = null)
        {
            var mutedRole = _guildService.GetMutedRole(guildId);
            if(mutedRole != null)
            {
                if (IsUserMuted(guildId, userId))
                    return false;

                var guild = _client.GetGuild(guildId);

                var mute = new Mute()
                {
                    GuildId = guildId,
                    MutedUserId = userId,
                    MutingUserId = adminId,
                    Reason = reason,
                    DateMuted = DateTime.UtcNow
                };

                if (length != null)
                    mute.DateUnmuted = (DateTime.UtcNow + length);

                var user = guild.GetUser(userId);
                using (var db = new DatabaseContext())
                {
                    db.Add(mute);
                    await user.AddRoleAsync(mutedRole);

                    if (user.VoiceChannel != null)
                        await user.ModifyAsync(_user => _user.Mute = true);

                    await db.SaveChangesAsync();
                    return true;
                }
            }

            return false;
        }

        // unmute user
        public async Task<bool> UnmuteUserAsync(ulong guildId, ulong userId)
        {
            var mutedRole = _guildService.GetMutedRole(guildId);
            if (mutedRole != null)
            {
                if (!IsUserMuted(guildId, userId))
                    return false;

                var guild = _client.GetGuild(guildId);
                var user = guild.GetUser(userId);

                using (var db = new DatabaseContext())
                {
                    if (!db.Mutes.Any(_mute => _mute.GuildId == guildId))
                        return false;

                    var mute = db.Mutes.Where(_mute => _mute.GuildId == guildId)
                                       .First(_mute => _mute.MutedUserId == userId);

                    if (user.Roles.Any(_role => _role.Id == mutedRole.Id))
                        await user.RemoveRoleAsync(mutedRole);

                    if (user.VoiceChannel != null)
                        await user.ModifyAsync(_user => _user.Mute = false);

                    db.Remove(mute);
                    await db.SaveChangesAsync();
                    return true;
                }
            }

            return false;
        }

        // terminate expired mutes
        public async void TerminateExpiredMutes(object sender, ElapsedEventArgs eventArgs)
        {
            using (var db = new DatabaseContext())
            {
                var expiredMutes = db.Mutes.Where(_mute => _mute.DateUnmuted != null && _mute.DateUnmuted <= DateTime.UtcNow).ToList();

                if (expiredMutes.Any())
                {
                    foreach (var mute in expiredMutes)
                    {
                        var guild = _client.GetGuild(mute.GuildId);
                        var user = guild.GetUser(mute.MutedUserId);
                        var logChannel = await guild.GetLoggingChannel();

                        var mutedRole = _guildService.GetMutedRole(guild.Id);
                        if (user != null && user.Roles.Any(_role => _role.Id == mutedRole.Id))
                        {
                            await user.RemoveRoleAsync(mutedRole);
                            if(user.VoiceChannel != null)
                                await user.ModifyAsync(_user => _user.Mute = false);

                            await (await user.GetOrCreateDMChannelAsync()).SendMessageAsync($":loud_sound: Your {(mute.DateUnmuted - mute.DateMuted).Value.Humanize()} mute has expired in {guild.Name}.");
                        }
                            
                        if(logChannel != null)
                            await logChannel.SendMessageAsync($":loud_sound: **{((user != null) ? user.ToString() : mute.MutedUserId.ToString())}'s** {(mute.DateUnmuted - mute.DateMuted).Value.Humanize()} mute has expired.");

                        db.Remove(mute);
                    }

                    await _logger.LogAsync(Discord.LogSeverity.Info, "Task::Mutes", $"Task ran successfully, terminating {expiredMutes.Count} expired mutes.");

                    try
                    {
                        await db.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException exception)
                    {
                        exception.Entries.ToList().ForEach(entry => entry.State = EntityState.Detached);
                    }
                }
            }
        }
    }
}
