﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace sXDB.Services
{
    public class LoggingService
    {
        private readonly DiscordSocketClient _client;
        private readonly CommandService _commandService;

        private string _logDir { get; }
        private string _logFile => Path.Combine(_logDir, $"{DateTime.UtcNow.ToString("yyyy-MM-dd")}.txt");

        public LoggingService(DiscordSocketClient client, CommandService commandService)
        {
            _logDir = Path.Combine(AppContext.BaseDirectory, "logs");

            _client = client;
            _commandService = commandService;

            _client.Log += LogAsync;
            _commandService.Log += LogAsync;
        }

        private Task LogAsync(LogMessage message)
        {
            if (!Directory.Exists(_logDir))
                Directory.CreateDirectory(_logDir);

            if (!File.Exists(_logFile))
                File.Create(_logFile).Dispose();

            string logString = $"{DateTime.UtcNow.ToString("hh:mm:ss")} [{message.Severity}] {message.Source}: '{message.Exception?.ToString() ?? message.Message}'";
            File.AppendAllText(_logFile, $"{logString}\n");

            return Console.Out.WriteLineAsync(logString);
        }

        public Task LogAsync(LogSeverity severity, string source, string message)
        {
            var logMessage = new LogMessage(severity, source, message);
            return LogAsync(logMessage);
        }

        public async Task LogToFileAsync(string fileName, string logContents, string consoleMessage)
        {
            if (!Directory.Exists(_logDir))
                Directory.CreateDirectory(_logDir);

            var filePath = Path.Combine(_logDir, $"{fileName}--{DateTime.UtcNow.ToString("yyyy-MM-dd-Thh-mm-ss")}.txt");
            if (!File.Exists(filePath))
                File.Create(filePath).Dispose();

            await File.AppendAllTextAsync(filePath, $"{consoleMessage}\r\n{logContents}");
            await LogAsync(LogSeverity.Error, "Exception Handler", consoleMessage);
        }

        public async Task<List<string>> TailLogsAsync(int requestedLines)
        {
            using (var streamReader = File.OpenText(_logFile))
            {
                var rawFile = await streamReader.ReadToEndAsync();

                var splitByLine = rawFile.Split("\n", StringSplitOptions.None);
                var lineCount = splitByLine.Count();
                if (lineCount < requestedLines)
                    return splitByLine.ToList();

                return splitByLine.Skip(Math.Max(0, lineCount - requestedLines)).ToList();
            }
        }
    }
}
