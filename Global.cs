﻿using System;
using Discord;

namespace sXDB
{
    public class Global
    {
        public static string Version = "0.2.4";

        public static string[] StaffRoles = { "Owners", "Admin", "Senior Moderator", "Moderator", "Trial-Mod" };

        private static uint[] Palette = { 0x00675A, 0x008A5E, 0x35C190, 0x77FAC6, 0x28C79F, 0x00A4AC, 0x007FA1, 0x375A7F, 0x536390, 0x726A9E, 0x9271A7, 0xB377AB, 0xD27DAA };
        public static Color ErrorColor = new Color(0xD13A28);

        public static Color RandomColorFromPalette()
        {
            var randomColor = Palette[(new Random()).Next(0, Palette.Length)];
            return new Color(randomColor);
        }
    }
}
