﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using sXDB.Core;
using sXDB.Database;
using sXDB.Services;
using System;
using System.Threading.Tasks;

namespace sXDB
{
    class Program
    {
        private DiscordSocketClient _client;
        private IServiceProvider serviceProvider;

        static void Main(string[] args) => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            var _config = new Configuration();
            await _config.Initialize();
            var coreConfig = await _config.Load();

            _client = new DiscordSocketClient(new DiscordSocketConfig
            {
                MessageCacheSize = 200,
                LogLevel = LogSeverity.Info,
                AlwaysDownloadUsers = true,
                ExclusiveBulkDelete = true // https://github.com/discord-net/Discord.Net/releases/tag/2.1.0
            });

            serviceProvider = BuildServiceCollection();

            var dbContext = new DatabaseContext();
            await dbContext.Database.EnsureCreatedAsync();

            var _eventHandler = serviceProvider.GetRequiredService<Core.EventHandler>();
            await _eventHandler.RegisterEvents();

            var _commandHandler = serviceProvider.GetRequiredService<CommandHandlingService>();
            await _commandHandler.Install();

            var _tasksService = serviceProvider.GetRequiredService<TasksService>();
            await _tasksService.CreateCoreTasks();

            var _storageService = serviceProvider.GetRequiredService<StorageService>();
            await _storageService.EnsureFolderExistence();

            var _queueService = serviceProvider.GetRequiredService<XMPQueueService>();
            await _queueService.CreateQueueTasks();

            // create the moderation tasks (removing expired temp bans/mutes)
            var _modService = serviceProvider.GetRequiredService<ModerationService>();
            await _modService.CreateModerationTasks();

            await _client.LoginAsync(TokenType.Bot, coreConfig.BotToken);
            await _client.StartAsync();

            await Task.Delay(-1);
        }

        private IServiceProvider BuildServiceCollection()
        {
            var services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton<CommandService>()
                .AddSingleton<CommandHandlingService>()
                .AddSingleton<LoggingService>()
                .AddSingleton<TasksService>()
                .AddSingleton<GuildService>()
                .AddSingleton<StorageService>()
                .AddSingleton<ChatService>()
                .AddSingleton<XMPQueueService>()
                .AddSingleton<ModerationService>()
                .AddSingleton<TagService>()
                .AddSingleton<Core.EventHandler>();

            return new DefaultServiceProviderFactory().CreateServiceProvider(services);
        }
    }
}
